package org.tinygroup.bizframe.service.impl;

import org.tinygroup.bizframe.basedao.util.PageResponseAdapter;
import org.tinygroup.bizframe.business.inter.SysRoleBusiness;
import org.tinygroup.bizframe.business.inter.SysRoleRightBusiness;
import org.tinygroup.bizframe.common.dto.PageRequest;
import org.tinygroup.bizframe.common.dto.PageResponse;
import org.tinygroup.bizframe.common.util.BeanUtil;
import org.tinygroup.bizframe.common.util.CamelCaseUtil;
import org.tinygroup.bizframe.dao.inter.constant.TsysRoleTable;
import org.tinygroup.bizframe.dao.inter.pojo.TsysRole;
import org.tinygroup.bizframe.dao.inter.pojo.complex.TRightRef;
import org.tinygroup.bizframe.service.inter.SysRoleService;
import org.tinygroup.bizframe.service.inter.dto.SysRoleDto;
import org.tinygroup.bizframe.service.inter.dto.complex.TRightRefDto;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

import java.util.List;

/**
 * Created by Mr.wang on 2016/7/7.
 */
public class SysRoleServiceImpl implements SysRoleService{
    private SysRoleBusiness sysRoleBusiness;

    private SysRoleRightBusiness sysRoleRightBusiness;

    public SysRoleRightBusiness getSysRoleRightBusiness() {
        return sysRoleRightBusiness;
    }

    public void setSysRoleRightBusiness(SysRoleRightBusiness sysRoleRightBusiness) {
        this.sysRoleRightBusiness = sysRoleRightBusiness;
    }

    public SysRoleBusiness getSysRoleBusiness() {
		return sysRoleBusiness;
	}

	public void setSysRoleBusiness(SysRoleBusiness sysRoleBusiness) {
		this.sysRoleBusiness = sysRoleBusiness;
	}

	public SysRoleDto getRole(Integer roleId) {
		TsysRole role = sysRoleBusiness.getById(roleId);
		SysRoleDto roleDto = new SysRoleDto();
        BeanUtil.copyProperties(roleDto,role);
        return roleDto;
    }

    public SysRoleDto addRole(SysRoleDto roleDto) {
        TsysRole role = BeanUtil.copyProperties(TsysRole.class,roleDto);
        return BeanUtil.copyProperties(SysRoleDto.class,sysRoleBusiness.add(role));
    }

    public int updateRole(SysRoleDto roleDto) {
        TsysRole role = BeanUtil.copyProperties(TsysRole.class,roleDto);
        return sysRoleBusiness.update(role);
    }

    public void deleteRoles(Integer[] ids) {
        sysRoleBusiness.deleteByKeys(ids);
    }

    public PageResponse getRolePager(PageRequest pageRequest, SysRoleDto roleDto) {
        TsysRole role = BeanUtil.copyProperties(TsysRole.class,roleDto);
        String sortField = pageRequest.getSort();
        if(StringUtil.isEmpty(sortField)){
            sortField= CamelCaseUtil.getFieldName("roleCode");
        }
        String orderByField= CamelCaseUtil.getFieldName(sortField);
        OrderBy orderBy=new OrderBy(orderByField,"asc".equalsIgnoreCase(pageRequest.getOrder()));

        Pager<TsysRole> rolePager = sysRoleBusiness.getPager(pageRequest.getStart(),pageRequest.getPageSize(),role,orderBy);
        return PageResponseAdapter.transform(rolePager);
    }

    public List getRoleList(SysRoleDto sysRoleDto) {
        TsysRole tsysRole = BeanUtil.copyProperties(TsysRole.class,sysRoleDto);
        return sysRoleBusiness.getList(tsysRole);
    }

    public boolean checkRoleExists(SysRoleDto roleDto) {
        TsysRole role = BeanUtil.copyProperties(TsysRole.class,roleDto);
        return sysRoleBusiness.checkExists(role);
    }

    public PageResponse queryRightsInRole(PageRequest pageRequest, Integer roleId, TRightRefDto tRightRefDto, Boolean isAssigned) {
        TRightRef tRightRef = BeanUtil.copyProperties(TRightRef.class,tRightRefDto);

        Pager pager =  sysRoleRightBusiness.queryRightsInRole(pageRequest.getStart(),pageRequest.getPageSize(),
                roleId,tRightRef,isAssigned);
        return PageResponseAdapter.transform(pager);
    }

    public boolean grantRoleRights(Integer roleId,Integer[] menuIds, String createBy) {
        sysRoleRightBusiness.grantRights(roleId,menuIds,createBy);
        return true;
    }

    public boolean revokeRoleRights(Integer roleId,Integer[] roleRightIds) {
        return sysRoleRightBusiness.revokeRights(roleId,roleRightIds);
    }


}
