package org.tinygroup.bizframe.service.impl;

import org.tinygroup.bizframe.business.inter.DictDataLoaderBusiness;
import org.tinygroup.bizframe.service.inter.DictLoaderService;
import org.tinygroup.dict.Dict;

import java.util.List;

/**
 * Created by wangwy on 2016/11/30.
 */
public class DictLoaderServiceImpl implements DictLoaderService {
    private DictDataLoaderBusiness dictDataLoaderBusiness;

    public DictDataLoaderBusiness getDictDataLoaderBusiness() {
        return dictDataLoaderBusiness;
    }

    public void setDictDataLoaderBusiness(DictDataLoaderBusiness dictDataLoaderBusiness) {
        this.dictDataLoaderBusiness = dictDataLoaderBusiness;
    }

    public List<Dict> loadDict() {
        return dictDataLoaderBusiness.loadDict();
    }
}
