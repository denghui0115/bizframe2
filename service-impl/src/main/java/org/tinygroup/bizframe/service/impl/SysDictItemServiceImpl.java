package org.tinygroup.bizframe.service.impl;

import org.tinygroup.bizframe.basedao.util.PageResponseAdapter;
import org.tinygroup.bizframe.business.inter.SysDictItemBusiness;
import org.tinygroup.bizframe.common.dto.PageRequest;
import org.tinygroup.bizframe.common.dto.PageResponse;
import org.tinygroup.bizframe.common.util.BeanUtil;
import org.tinygroup.bizframe.common.util.CamelCaseUtil;
import org.tinygroup.bizframe.dao.inter.pojo.TsysDictItem;
import org.tinygroup.bizframe.dao.inter.constant.TsysDictItemTable;
import org.tinygroup.bizframe.service.inter.SysDictItemService;
import org.tinygroup.bizframe.service.inter.dto.SysDictItemDto;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

/**
 * Created by Mr.wang on 2016/7/21.
 */
public class SysDictItemServiceImpl implements SysDictItemService{
	private SysDictItemBusiness sysDictItemBusiness;

    public SysDictItemBusiness getSysDictItemBusiness() {
		return sysDictItemBusiness;
	}

	public void setSysDictItemBusiness(SysDictItemBusiness sysDictItemBusiness) {
		this.sysDictItemBusiness = sysDictItemBusiness;
	}

	public SysDictItemDto getDictItem(Integer DictItemId) {
		TsysDictItem DictItem = sysDictItemBusiness.getById(DictItemId);
		SysDictItemDto DictItemDto = new SysDictItemDto();
        BeanUtil.copyProperties(DictItemDto,DictItem);
        return DictItemDto;
    }

    public SysDictItemDto addDictItem(SysDictItemDto DictItemDto) {
        TsysDictItem DictItem = BeanUtil.copyProperties(TsysDictItem.class,DictItemDto);
        return BeanUtil.copyProperties(SysDictItemDto.class,sysDictItemBusiness.add(DictItem));
    }

    public int updateDictItem(SysDictItemDto DictItemDto) {
        TsysDictItem DictItem = BeanUtil.copyProperties(TsysDictItem.class,DictItemDto);
        return sysDictItemBusiness.update(DictItem);
    }

    public void deleteDictItems(Integer[] ids) {
        sysDictItemBusiness.deleteByKeys(ids);
    }

    public PageResponse getDictItemPager(PageRequest pageRequest, SysDictItemDto dictItemDto) {
    	
        TsysDictItem dictItem = BeanUtil.copyProperties(TsysDictItem.class,dictItemDto);
        String sortField = pageRequest.getSort();
        if(StringUtil.isEmpty(sortField)){
            sortField= CamelCaseUtil.getFieldName("id");
        }
        String orderByField=CamelCaseUtil.getFieldName(sortField);
        OrderBy orderBy=new OrderBy(orderByField,"asc".equalsIgnoreCase(pageRequest.getOrder()));
        
		Pager<TsysDictItem> dictItemPager = sysDictItemBusiness.getPager(pageRequest.getStart(),pageRequest.getPageSize(),dictItem,orderBy);
        return PageResponseAdapter.transform(dictItemPager);
    }

    public boolean checkDictItemExists(SysDictItemDto dictItemDto) {
        TsysDictItem dictItem = BeanUtil.copyProperties(TsysDictItem.class,dictItemDto);
        return sysDictItemBusiness.checkExists(dictItem);
    }

}
