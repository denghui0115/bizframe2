/**
 *
 * Copyright (c) 2014-2017 All Rights Reserved.
 */
package org.tinygroup.bizframe.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.tinygroup.bizframe.business.inter.FullTextBusiness;
import org.tinygroup.bizframe.common.dto.PageRequest;
import org.tinygroup.bizframe.common.dto.PageResponse;
import org.tinygroup.bizframe.service.inter.FullTextService;
import org.tinygroup.fulltext.FullText;
import org.tinygroup.fulltext.FullTextHelper;
import org.tinygroup.fulltext.document.DefaultDocument;
import org.tinygroup.fulltext.document.Document;
import org.tinygroup.fulltext.field.StringField;
import org.tinygroup.fulltext.impl.DefaultSearchRule;

/**
 * 全文检索服务
 * @author zhangliang08072
 * @version $Id: FullTextServiceImpl.java, v 0.1 2017-4-13 上午9:40:12 zhangliang08072 Exp $
 */
public class FullTextServiceImpl implements FullTextService {
	
	private FullTextBusiness fullTextBusiness;
	

	public FullTextBusiness getFullTextBusiness() {
		return fullTextBusiness;
	}


	public void setFullTextBusiness(FullTextBusiness fullTextBusiness) {
		this.fullTextBusiness = fullTextBusiness;
	}


	/** 
	 * @see org.tinygroup.bizframe.service.inter.FullTextService#getPagerFullText(org.tinygroup.bizframe.common.dto.PageRequest, java.lang.String, java.lang.Class, java.lang.String, java.lang.String)
	 */
	@Override
	public PageResponse getPagerFullText(PageRequest pageRequest,
			String searchText, Class objClass, String tableName,
			String columnName,String fieldType,Boolean isIdString) {
		FullText fullText = FullTextHelper.getFullText();
		DefaultSearchRule searchRule = new DefaultSearchRule();
		searchRule.addField(FullTextHelper.getStoreType(), fieldType);
		
		searchText = searchText.trim();
		String wildcardSearchText = searchText+" | "+searchText+"*";
		if(searchText.indexOf("*")>=0){
			searchRule.setDefault(searchText);
	     }else{
	    	 searchRule.setDefault(wildcardSearchText);  //同时支持精确匹配和通配符模式
	     }
		
		org.tinygroup.fulltext.Pager<Document> documentsPager = fullText.search(searchRule, pageRequest.getStart(), pageRequest.getPageSize());
		List<Document> documents = documentsPager.getRecords();
		PageResponse reponse;
		reponse = transform(documentsPager);
		
		if(documents!=null && documents.size()>0){
			String[] idArray = new String[documents.size()];
			for(int i = 0;i<documents.size();i++){
				idArray[i]= documents.get(i).getId().getValue().toString().replace(tableName+"_", "");
			}
			List objList = fullTextBusiness.queryOjbListByIds(objClass, idArray,isIdString, tableName, columnName);
			reponse.setRows(objList);
		}
		return reponse;
	}
	
	/** 
	 * @see org.tinygroup.bizframe.service.inter.FullTextService#addFullTextIndex(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void addFullTextIndex(String _id, String _type, String _abstract) {
		FullText fullText = FullTextHelper.getFullText();
		List<Document> docs = createDocumentList(_id,  _type, null, _abstract);
        fullText.createIndex( _type, docs);
	}
	

	/** 
	 * @see org.tinygroup.bizframe.service.inter.FullTextService#deleteFullTextIndex(java.lang.String[])
	 */
	@Override
	public void deleteFullTextIndex(String _type ,String[] idArray) {
		FullText fullText = FullTextHelper.getFullText();
		List<Document> docs = createDeleteDocumentList(idArray);
		fullText.deleteIndex(_type, docs);
	}
	
	
	
	public static PageResponse transform(org.tinygroup.fulltext.Pager pager){
        PageResponse response = new PageResponse();
        //response.setRows(pager.getRecords());
        response.setTotalPages(pager.getTotalPages());
        response.setTotal(pager.getTotalCount());
        response.setPageNo(pager.getCurrentPage());
        response.setStart(pager.getStart());
        response.setPageSize(pager.getLimit());
        return response;
    }
	
	public static List<Document> createDocumentList(String idValue,String typeValue,String titleValue,String abstractValue){
    	List<Document> docList = new ArrayList<Document>();
    	DefaultDocument document  = new DefaultDocument();
    	StringField idField = new StringField(FullTextHelper.getStoreId(),idValue);
    	StringField typeField = new StringField(FullTextHelper.getStoreType(),typeValue);
    	StringField titleField = new StringField(FullTextHelper.getStoreTitle(),titleValue,true,true,true);
    	StringField abstractField = new StringField(FullTextHelper.getStoreAbstract(),abstractValue,true,true,true);
    	
    	document.addField(idField);
    	document.addField(typeField);
    	document.addField(titleField);
    	document.addField(abstractField);
    	
    	docList.add(document);
    	return docList;
    }

    public static List<Document> createDeleteDocumentList(String[] idValues){
    	List<Document> docs = new ArrayList<Document>();
    	for(int i =0;i<idValues.length;i++){
			StringField idField = new StringField(FullTextHelper.getStoreId(),idValues[i]);
			DefaultDocument doc = new DefaultDocument();
			doc.addField(idField);
			docs.add(doc);
    	}
    	return docs;
    }


	/** 
	 * @see org.tinygroup.bizframe.service.inter.FullTextService#getObjectByKey(java.lang.Class, java.lang.String, java.lang.Boolean, java.lang.String, java.lang.String)
	 */
	@Override
	public Serializable getBusinessObjectByKey(Class objClass, String id,
			Boolean isIdString, String tableName, String columnName) {
		return (Serializable)fullTextBusiness.queryOjbById(objClass, id, isIdString, tableName, columnName);
	}

}
