package org.tinygroup.bizframe.service.impl;

import org.tinygroup.bizframe.basedao.util.PageResponseAdapter;
import org.tinygroup.bizframe.business.inter.SysDepBusiness;
import org.tinygroup.bizframe.common.dto.PageRequest;
import org.tinygroup.bizframe.common.dto.PageResponse;
import org.tinygroup.bizframe.common.util.BeanUtil;
import org.tinygroup.bizframe.common.util.CamelCaseUtil;
import org.tinygroup.bizframe.dao.inter.constant.TsysDepTable;
import org.tinygroup.bizframe.dao.inter.pojo.TreeData;
import org.tinygroup.bizframe.dao.inter.pojo.TsysDep;
import org.tinygroup.bizframe.service.inter.SysDepService;
import org.tinygroup.bizframe.service.inter.dto.SysDepDto;
import org.tinygroup.bizframe.service.inter.dto.TreeDto;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.logger.Logger;
import org.tinygroup.logger.LoggerFactory;
import org.tinygroup.tinysqldsl.Pager;

import java.util.List;

/**
 * Created by Mr.wang on 2016/7/14.
 */
public class SysDepServiceImpl implements SysDepService {

    private static final Logger logger = LoggerFactory.getLogger(SysDepServiceImpl.class);

    private SysDepBusiness sysDepBusiness;

    public SysDepBusiness getSysDepBusiness() {
        return sysDepBusiness;
    }

    public void setSysDepBusiness(SysDepBusiness sysDepBusiness) {
        this.sysDepBusiness = sysDepBusiness;
    }

    public SysDepDto getSysDep(String depCode) {
        return BeanUtil.copyProperties(SysDepDto.class, sysDepBusiness.getById(depCode));
    }

    public SysDepDto addSysDep(SysDepDto sysDepDto) {
        TsysDep tsysdep = BeanUtil.copyProperties(TsysDep.class, sysDepDto);
        return BeanUtil.copyProperties(SysDepDto.class, sysDepBusiness.add(tsysdep));
    }

    public int updateSysDep(SysDepDto sysDepDto) {
        TsysDep tsysdep = BeanUtil.copyProperties(TsysDep.class, sysDepDto);
        return sysDepBusiness.update(tsysdep);
    }

    public int deleteSysDep(String[] depCode) {
        return sysDepBusiness.deleteByKeys(depCode);
    }

    public PageResponse getSysDepPager(PageRequest pageRequest, SysDepDto sysDepDto) {
        TsysDep tsysdep = BeanUtil.copyProperties(TsysDep.class, sysDepDto);

        String sortField = pageRequest.getSort();
        if (StringUtil.isEmpty(sortField)) {
            sortField = CamelCaseUtil.getFieldName("depCode");
        }
        String orderByField = CamelCaseUtil.getFieldName(sortField);
        boolean isAsc = "asc".equalsIgnoreCase(pageRequest.getOrder());
        logger.warnMessage("Is ASC {0}", isAsc);
        OrderBy orderBy = new OrderBy(orderByField, isAsc);

        Pager<TsysDep> tsysDepPager = sysDepBusiness
                .getPager(pageRequest.getStart(), pageRequest.getPageSize(), tsysdep, orderBy);
        return PageResponseAdapter.transform(tsysDepPager);
    }

    public List getSysDepList(SysDepDto sysDepDto) {
        if (sysDepDto == null) {
            sysDepDto = new SysDepDto();
        }
        TsysDep tsysdep = BeanUtil.copyProperties(TsysDep.class, sysDepDto);
        return sysDepBusiness.getList(tsysdep);
    }

    public boolean checkSysDepExists(SysDepDto sysDepDto) {
        TsysDep tsysdep = BeanUtil.copyProperties(TsysDep.class, sysDepDto);
        return sysDepBusiness.checkExists(tsysdep);
    }

    public List getDepTreeData(TreeDto treeDto) {
        TreeData tree = BeanUtil.copyProperties(TreeData.class, treeDto);
        return sysDepBusiness.getTreeData(tree);
    }

    public List getDepTreeByBranch(TreeDto treeDto) {
        TreeData tree = BeanUtil.copyProperties(TreeData.class, treeDto);
        return sysDepBusiness.getDepTreeByBranch(tree);
    }

    public List getParentDepTreeByBranch(TreeDto treeDto) {
        String id = treeDto.getId();
        treeDto.setId(null);
        TreeData tree = BeanUtil.copyProperties(TreeData.class, treeDto);
        List<TreeData> depTreeList = sysDepBusiness.getDepTreeByBranch(tree);
        //为当前节点设置disableClick、disableSub
        for (TreeData depTree : depTreeList) {
            if (depTree.getId().equals(id)) {
                depTree.setDisableClick("true");
                depTree.setDisableSub("true");
                break;
            }
        }
        return depTreeList;
    }
}
