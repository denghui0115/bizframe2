package org.tinygroup.bizframe.business.inter;

import java.util.List;

import org.tinygroup.bizframe.dao.inter.pojo.TreeData;
import org.tinygroup.bizframe.dao.inter.pojo.TsysMenu;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

/**
 * Created by Mr.wang on 2016/7/14.
 */
public interface SysMenuBusiness {
	TsysMenu getById(Integer id);

    Pager<TsysMenu> getPager(int start, int limit, TsysMenu sysMenu, final OrderBy... orderBies);

    TsysMenu add(TsysMenu sysMenu);

    int update(TsysMenu sysMenu);

    int deleteByKeys(Integer... pks);

    boolean checkExists(TsysMenu sysMenu);
    
    List<TsysMenu> getMenuInfos(TsysMenu sysMenu);

	List<TsysMenu> getMenuTree(TreeData tree);

    List<TsysMenu> getSysMenuList(TsysMenu tsysMenu);

    List<Integer> findMenuIdsByUser(String userCode, OrderBy... orderArgs);

}
