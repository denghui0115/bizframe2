package org.tinygroup.bizframe.business.inter;

import org.tinygroup.bizframe.ext.dao.inter.pojo.TsysRoleRight;
import org.tinygroup.bizframe.dao.inter.pojo.complex.TRightRef;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

/**
 * Created by Mr.wang on 2016/7/14.
 */
public interface SysRoleRightBusiness {
	TsysRoleRight getById(Integer id);

    Pager<TsysRoleRight> getPager(int start, int limit, TsysRoleRight sysRoleRight, final OrderBy... orderBies);

    TsysRoleRight add(TsysRoleRight sysRoleRight);

    int update(TsysRoleRight sysRoleRight);

    int deleteByKeys(Integer... pks);

    boolean checkExists(TsysRoleRight sysRoleRight);

    Pager<TsysRoleRight> queryRightsInRole(int start, int pageSize, Integer roleId, TRightRef tRightRef, Boolean isAssigned);

    boolean grantRights(Integer roleId,Integer[] menuIds, String createBy);

    boolean revokeRights(Integer roleId,Integer[] roleRightIds);
}
