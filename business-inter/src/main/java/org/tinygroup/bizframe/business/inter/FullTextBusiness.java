/**
 *
 * Copyright (c) 2014-2017 All Rights Reserved.
 */
package org.tinygroup.bizframe.business.inter;

import java.util.List;

/**
 * 
 * @author zhangliang08072
 * @version $Id: FullTextBusiness.java, v 0.1 2017-4-13 上午9:55:57 zhangliang08072 Exp $
 */
public interface FullTextBusiness {
	public <T> List<T> queryOjbListByIds(Class<T> objClass,String[] idArray,Boolean isIdString, String tableName, String columnName);
	
	public <T> T queryOjbById(Class<T> objClass, String id, Boolean isIdString,String tableName, String columnName);
}
