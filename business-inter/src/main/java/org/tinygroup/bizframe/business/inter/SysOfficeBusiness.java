package org.tinygroup.bizframe.business.inter;

import java.util.List;

import org.tinygroup.bizframe.dao.inter.pojo.TreeData;
import org.tinygroup.bizframe.dao.inter.pojo.TsysOffice;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

/**
 * Created by Mr.wang on 2016/7/12.
 */
public interface SysOfficeBusiness {
	TsysOffice getById(String officeCode);

    Pager<TsysOffice> getPager(int start, int limit, TsysOffice sysOffice, final OrderBy... orderBies);

    TsysOffice add(TsysOffice sysOffice);

    int update(TsysOffice sysOffice);

    int deleteByKeys(String... pks);

    boolean checkExists(TsysOffice sysOffice);
    
    List<TreeData> getOfficeTree(TreeData tree);
    
    List<TreeData> getOfficeTreeByDep(TreeData tree);

	List<TsysOffice> getOfficeList(TsysOffice office);

//    List<TreeData> getOfficeTree(TsysOffice tsysOffice);
}
