package org.tinygroup.bizframe.common.security;
/**
 * 
 * @author Mr.wang
 * 加密接口
 */
public interface Encryption {
	String execute(String inputStr);
}
