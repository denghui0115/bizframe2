/**
 * Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 * <p>
 * Licensed under the GPL, Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/gpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tinygroup.bizframe.aop.processor;

import org.aopalliance.intercept.MethodInvocation;
import org.tinygroup.bizframe.aop.config.FullTextSearchAction;
import org.tinygroup.bizframe.aop.config.base.ExtensionAction;
import org.tinygroup.bizframe.aop.config.base.SearchTypeTableItem;
import org.tinygroup.bizframe.aop.exception.AopExtensionException;
import org.tinygroup.bizframe.aop.processor.base.AbstractExtensionActionProcessor;
import org.tinygroup.bizframe.common.dto.PageRequest;
import org.tinygroup.bizframe.service.inter.FullTextService;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.logger.LogLevel;
import org.tinygroup.logger.Logger;
import org.tinygroup.logger.LoggerFactory;

/**
 * aop全文检索查询
 *
 */
public class FullTextSearchProcessor extends AbstractExtensionActionProcessor {
	
	protected static final Logger LOGGER = LoggerFactory.getLogger(FullTextSearchProcessor.class);
	
	private FullTextService fullTextService;
	
	
	public FullTextService getFullTextService() {
		return fullTextService;
	}

	public void setFullTextService(FullTextService fullTextService) {
		this.fullTextService = fullTextService;
	}

	@Override
    public boolean preProcess(ExtensionAction action, MethodInvocation invocation) {
		if (!(action instanceof FullTextSearchAction)) {
            throw new AopExtensionException("当前处理器[%s]不匹配扩展动作[%s]", this.getClass().getName(), action.getClass().getName());
        }
		FullTextSearchAction fullTextSearchAction = (FullTextSearchAction)action;
		Object[] params = invocation.getArguments();
		Object oSearchText = params[fullTextSearchAction.getSearchTextIndex()];
		String searchText = oSearchText==null?"":(String)oSearchText;
		return StringUtil.isBlank(searchText);
    }
	
	@Override
    public Object endProcessor(ExtensionAction action, MethodInvocation invocation) {
		LOGGER.logMessage(LogLevel.DEBUG, "=====================>全文检索aop拦截执行成功！");
		if (!(action instanceof FullTextSearchAction)) {
            throw new AopExtensionException("当前处理器[%s]不匹配扩展动作[%s]", this.getClass().getName(), action.getClass().getName());
        }
		FullTextSearchAction fullTextSearchAction = (FullTextSearchAction)action;
		Object[] params = invocation.getArguments();
		Object oSearchText = params[fullTextSearchAction.getSearchTextIndex()];
		String searchText = oSearchText==null?"":(String)oSearchText;
		PageRequest pageRequest = (PageRequest)params[fullTextSearchAction.getPageRequestindex()];
		String searchType = fullTextSearchAction.getSearchType();
		SearchTypeTableItem item = extensionActionXmlManager.getSearchTypeTableItemByType(searchType);
		String tableName = item.getTableName();
		String className = item.getClassName();
		String columnName = item.getColumnName();
		Boolean isIdString = item.getIsIdString();
		Class objClass = null;
		try {
			objClass = Class.forName(className);
		} catch (ClassNotFoundException e) {
			throw new AopExtensionException(e.getMessage());
		}
		return fullTextService.getPagerFullText(pageRequest, searchText, objClass, tableName, columnName, searchType,isIdString);
    }
}
