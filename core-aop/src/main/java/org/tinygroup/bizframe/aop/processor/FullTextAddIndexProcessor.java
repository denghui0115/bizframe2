/**
 * Copyright (c) 2012-2016, www.tinygroup.org (luo_guo@icloud.com).
 * <p>
 * Licensed under the GPL, Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/gpl.html
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tinygroup.bizframe.aop.processor;

import org.aopalliance.intercept.MethodInvocation;
import org.tinygroup.bizframe.aop.config.FullTextAddIndexAction;
import org.tinygroup.bizframe.aop.config.base.ExtensionAction;
import org.tinygroup.bizframe.aop.exception.AopExtensionException;
import org.tinygroup.bizframe.aop.processor.base.AbstractExtensionActionProcessor;
import org.tinygroup.bizframe.service.inter.FullTextService;
import org.tinygroup.logger.LogLevel;
import org.tinygroup.logger.Logger;
import org.tinygroup.logger.LoggerFactory;
import org.tinygroup.template.TemplateContext;

/**
 * aop全文检索新增索引
 *
 */
public class FullTextAddIndexProcessor extends AbstractExtensionActionProcessor {
	
	protected static final Logger LOGGER = LoggerFactory.getLogger(FullTextAddIndexProcessor.class);
	
	private FullTextService fullTextService;
	
	
	public FullTextService getFullTextService() {
		return fullTextService;
	}

	public void setFullTextService(FullTextService fullTextService) {
		this.fullTextService = fullTextService;
	}
	
	@Override
    public void postProcess(ExtensionAction action, MethodInvocation invocation, Object result) {
		LOGGER.logMessage(LogLevel.DEBUG, "=====================>全文检索新增索引aop拦截执行成功！");
		if (!(action instanceof FullTextAddIndexAction)) {
            throw new AopExtensionException(String.format("当前处理器[%s]不匹配扩展动作[%s]", this.getClass().getName(), action.getClass().getName()));
        }
		FullTextAddIndexAction fullTextAddIndexAction = (FullTextAddIndexAction)action;
		TemplateContext templateContext = templateRender.assemblyContext(invocation);
		templateContext.put("result", result);
		
		try{
			String searchId = templateRender.renderTemplate(templateContext,fullTextAddIndexAction.getSearchId());
			String searchType = fullTextAddIndexAction.getSearchType();
			String searchAbstract = templateRender.renderTemplate(templateContext,fullTextAddIndexAction.getSearchAbstract());
			fullTextService.addFullTextIndex(searchId, searchType, searchAbstract);
		}catch (Exception e) {
            throw new AopExtensionException(e);
        }
    }
}
