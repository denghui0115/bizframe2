
package org.tinygroup.bizframe.aop.config;

import org.tinygroup.bizframe.aop.config.base.ExtensionAction;
import org.tinygroup.bizframe.aop.processor.FullTextDeleteIndexProcessor;
import org.tinygroup.bizframe.aop.processor.base.ExtensionActionProcessor;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("fulltext-delete-index-action")
public class FullTextDeleteIndexAction extends ExtensionAction {    
    @XStreamAsAttribute
    @XStreamAlias("search-id")
    private String searchId;
    

    @Override
    public Class<? extends ExtensionActionProcessor> getProcessorClass() {
        return FullTextDeleteIndexProcessor.class;
    }

	public String getSearchId() {
		return searchId;
	}

	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}
}
