package org.tinygroup.bizframe.aop.processor.base;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.beans.factory.InitializingBean;
import org.tinygroup.bizframe.aop.config.base.ExtensionAction;
import org.tinygroup.bizframe.aop.interceptor.ExtensionInterceptor;
import org.tinygroup.bizframe.aop.template.ExtensionTemplateRender;
import org.tinygroup.bizframe.aop.xmlfileparser.ExtensionActionXmlManager;
import org.tinygroup.commons.tools.Assert;

public abstract class AbstractExtensionActionProcessor implements ExtensionActionProcessor, InitializingBean {
    protected ExtensionTemplateRender templateRender;
    private ExtensionInterceptor interceptor;
    protected ExtensionActionXmlManager extensionActionXmlManager;

    public ExtensionTemplateRender getTemplateRender() {
		return templateRender;
	}

	public void setTemplateRender(ExtensionTemplateRender templateRender) {
		this.templateRender = templateRender;
	}
	
	public ExtensionInterceptor getInterceptor() {
		return interceptor;
	}

	public void setInterceptor(ExtensionInterceptor interceptor) {
		this.interceptor = interceptor;
	}
	

	public ExtensionActionXmlManager getExtensionActionXmlManager() {
		return extensionActionXmlManager;
	}

	public void setExtensionActionXmlManager(
			ExtensionActionXmlManager extensionActionXmlManager) {
		this.extensionActionXmlManager = extensionActionXmlManager;
	}

	@Override
    public void afterPropertiesSet() throws Exception {
        Assert.assertNotNull(interceptor, "ExtensionInterceptor must not be null");
        interceptor.addProcessor(this);
    }

    @Override
    public boolean preProcess(ExtensionAction action, MethodInvocation invocation) {
        return true;
    }

    @Override
    public void postProcess(ExtensionAction action, MethodInvocation invocation, Object result) {

    }

    @Override
    public Object endProcessor(ExtensionAction action, MethodInvocation invocation) {
        return null;
    }
}
