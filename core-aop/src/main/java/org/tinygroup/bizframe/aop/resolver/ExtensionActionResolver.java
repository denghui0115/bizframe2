package org.tinygroup.bizframe.aop.resolver;

import java.lang.reflect.Method;
import java.util.List;

import org.springframework.core.Ordered;
import org.tinygroup.bizframe.aop.config.base.ExtensionAction;

public interface ExtensionActionResolver extends Ordered {
    /**
     * 解析扩展动作
     *
     * @param method 针对的方法
     * @return 解析方法上所有的扩展动作
     */
    List<ExtensionAction> resolve(Method method);
}