package org.tinygroup.bizframe.aop.config.base;

import org.tinygroup.bizframe.aop.processor.base.ExtensionActionProcessor;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public abstract class ExtensionAction {
	
	@XStreamAsAttribute
    @XStreamAlias("search-type")
    private String searchType;
	
	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

    /**
     * 获取动作对应的处理器class类型
     *
     * @return
     */
    public abstract Class<? extends ExtensionActionProcessor> getProcessorClass();
}
