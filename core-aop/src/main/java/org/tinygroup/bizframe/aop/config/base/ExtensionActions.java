package org.tinygroup.bizframe.aop.config.base;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("extension-actions")
public class ExtensionActions {

    @XStreamImplicit
    private List<ExtensionAction> actions;

    public List<ExtensionAction> getActions() {
        if (actions == null) {
            actions = new ArrayList<ExtensionAction>();
        }
        return actions;
    }

    public void setActions(List<ExtensionAction> actions) {
        this.actions = actions;
    }
}
