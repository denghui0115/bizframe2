package org.tinygroup.bizframe.aop.template;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.core.ParameterNameDiscoverer;
import org.tinygroup.template.TemplateEngine;
import org.tinygroup.template.loader.StringResourceLoader;

public class ExtensionTemplateRenderFactoryBean implements FactoryBean<ExtensionTemplateRender> {
    private TemplateEngine templateEngine;
    private ParameterNameDiscoverer parameterNameDiscoverer;

    public void setTemplateEngine(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public void setParameterNameDiscoverer(ParameterNameDiscoverer parameterNameDiscoverer) {
        this.parameterNameDiscoverer = parameterNameDiscoverer;
    }

    @Override
    public ExtensionTemplateRender getObject() throws Exception {
        StringResourceLoader stringResourceLoader = new StringResourceLoader();
        templateEngine.addResourceLoader(stringResourceLoader);
        ExtensionTemplateRender templateRender = new ExtensionTemplateRender();
        templateRender.setParameterNameDiscoverer(parameterNameDiscoverer);
        templateRender.setResourceLoader(stringResourceLoader);
        return templateRender;
    }

    @Override
    public Class<?> getObjectType() {
        return ExtensionTemplateRender.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
