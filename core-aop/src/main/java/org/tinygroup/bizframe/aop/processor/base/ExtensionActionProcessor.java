package org.tinygroup.bizframe.aop.processor.base;

import org.aopalliance.intercept.MethodInvocation;
import org.tinygroup.bizframe.aop.config.base.ExtensionAction;


public interface ExtensionActionProcessor {
    /**
     * 前置处理操作
     *
     * @param action
     * @param invocation
     * @return true:可以执行下一个处理器,false:认为流程已经执行完毕，调用{@link #endProcessor(ExtensionAction, MethodInvocation)}  }
     */
    boolean preProcess(ExtensionAction action, MethodInvocation invocation);

    /**
     * 后置处理操作
     *
     * @param action
     * @param invocation
     * @param result
     */
    void postProcess(ExtensionAction action, MethodInvocation invocation, Object result);

    /**
     * 流程结束操作
     *
     * @param action
     * @param invocation
     * @return 方法返回值作为  {@link org.tinygroup.ExtensionInterceptor.base.aop.FoundationExtensionInterceptor#invoke(MethodInvocation)} 方法的返回值
     */
    Object endProcessor(ExtensionAction action, MethodInvocation invocation);
}
