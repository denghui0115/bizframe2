package org.tinygroup.bizframe.aop.exception;

import org.tinygroup.context.Context;
import org.tinygroup.exception.BaseRuntimeException;

import java.util.Locale;

public class AopExtensionException extends BaseRuntimeException {

	private static final long serialVersionUID = 4253174924424418016L;

	public AopExtensionException() {
        super();
    }

    public AopExtensionException(String errorCode, Context context, Locale locale) {
        super(errorCode, context, locale);
    }

    public AopExtensionException(String errorCode, Context context) {
        super(errorCode, context);
    }

    public AopExtensionException(String errorCode, Object... params) {
        super(errorCode, params);
    }

    public AopExtensionException(String errorCode, String defaultErrorMsg,
                                  Context context, Locale locale) {
        super(errorCode, defaultErrorMsg, context, locale);
    }

    public AopExtensionException(String errorCode, String defaultErrorMsg,
                                  Locale locale, Object... params) {
        super(errorCode, defaultErrorMsg, locale, params);
    }

    public AopExtensionException(String errorCode, String defaultErrorMsg,
                                  Locale locale, Throwable throwable, Object... params) {
        super(errorCode, defaultErrorMsg, locale, throwable, params);
    }

    public AopExtensionException(String errorCode, String defaultErrorMsg,
                                  Throwable throwable, Object... params) {
        super(errorCode, defaultErrorMsg, throwable, params);
    }

    public AopExtensionException(String errorCode, Throwable throwable,
                                  Object... params) {
        super(errorCode, throwable, params);
    }

    public AopExtensionException(Exception cause) {
        super(cause);
    }

}
