
package org.tinygroup.bizframe.aop.config;

import org.tinygroup.bizframe.aop.config.base.ExtensionAction;
import org.tinygroup.bizframe.aop.processor.FullTextSearchProcessor;
import org.tinygroup.bizframe.aop.processor.base.ExtensionActionProcessor;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("fulltext-search-action")
public class FullTextSearchAction extends ExtensionAction {

    @XStreamAsAttribute
    @XStreamAlias("search-text-index")
    private Integer searchTextIndex;
    
    @XStreamAsAttribute
    @XStreamAlias("page-request-index")
    private Integer pageRequestindex;

    @Override
    public Class<? extends ExtensionActionProcessor> getProcessorClass() {
        return FullTextSearchProcessor.class;
    }

	public Integer getSearchTextIndex() {
		return searchTextIndex;
	}

	public void setSearchTextIndex(Integer searchTextIndex) {
		this.searchTextIndex = searchTextIndex;
	}

	public Integer getPageRequestindex() {
		return pageRequestindex;
	}

	public void setPageRequestindex(Integer pageRequestindex) {
		this.pageRequestindex = pageRequestindex;
	}
}
