
package org.tinygroup.bizframe.aop.config;

import org.tinygroup.bizframe.aop.config.base.ExtensionAction;
import org.tinygroup.bizframe.aop.processor.FullTextAddIndexProcessor;
import org.tinygroup.bizframe.aop.processor.base.ExtensionActionProcessor;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("fulltext-add-index-action")
public class FullTextAddIndexAction extends ExtensionAction {
    
    @XStreamAsAttribute
    @XStreamAlias("search-id")
    private String searchId;
    
    @XStreamAsAttribute
    @XStreamAlias("search-abstract")
    private String searchAbstract;

    @Override
    public Class<? extends ExtensionActionProcessor> getProcessorClass() {
        return FullTextAddIndexProcessor.class;
    }

	public String getSearchId() {
		return searchId;
	}

	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}

	public String getSearchAbstract() {
		return searchAbstract;
	}

	public void setSearchAbstract(String searchAbstract) {
		this.searchAbstract = searchAbstract;
	}
}
