package org.tinygroup.bizframe.aop.interceptor;

import org.tinygroup.bizframe.aop.config.base.ExtensionAction;
import org.tinygroup.bizframe.aop.processor.base.ExtensionActionProcessor;

public class ExtensionHolder {
    private ExtensionAction action;
    private ExtensionActionProcessor processor;

    public ExtensionHolder() {
        super();
    }

    public ExtensionHolder(ExtensionAction action, ExtensionActionProcessor processor) {
        super();
        this.action = action;
        this.processor = processor;
    }

    public ExtensionAction getAction() {
        return action;
    }

    public void setAction(ExtensionAction action) {
        this.action = action;
    }

    public ExtensionActionProcessor getProcessor() {
        return processor;
    }

    public void setProcessor(ExtensionActionProcessor processor) {
        this.processor = processor;
    }
}
