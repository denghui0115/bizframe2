package org.tinygroup.bizframe.aop.template;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.core.ParameterNameDiscoverer;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.template.Template;
import org.tinygroup.template.TemplateContext;
import org.tinygroup.template.TemplateException;
import org.tinygroup.template.impl.TemplateContextDefault;
import org.tinygroup.template.loader.StringResourceLoader;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;

public class ExtensionTemplateRender {
    private StringResourceLoader resourceLoader;
    private ParameterNameDiscoverer parameterNameDiscoverer;

    public void setResourceLoader(StringResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public void setParameterNameDiscoverer(ParameterNameDiscoverer parameterNameDiscoverer) {
        this.parameterNameDiscoverer = parameterNameDiscoverer;
    }
    
    public String renderTemplate(MethodInvocation invocation, String content)
            throws TemplateException, UnsupportedEncodingException {
        if (StringUtil.isBlank(content)) {
            return "";
        }
        TemplateContext templateContext = assemblyContext(invocation);
        return renderTemplate(templateContext, content);
    }

    public String renderTemplate(TemplateContext context, String content)
            throws TemplateException, UnsupportedEncodingException {
        if (StringUtil.isBlank(content)) {
            return "";
        }
        Template template = resourceLoader.loadTemplate(content);
        ByteArrayOutputStream writer = new ByteArrayOutputStream();
        template.render(context, writer);
        return new String(writer.toByteArray(), "UTF-8");
    }

    public TemplateContext assemblyContext(MethodInvocation invocation) {
        TemplateContext context = new TemplateContextDefault();
        Method method = invocation.getMethod();
        String[] paramNames = parameterNameDiscoverer.getParameterNames(method);
        if (paramNames != null) {
            for (int i = 0; i < paramNames.length; i++) {
                context.put(paramNames[i], invocation.getArguments()[i]);
            }
        }
        return context;
    }

    public Object getParamValue(TemplateContext context, String key) {
        return context.get(key);
    }
}
