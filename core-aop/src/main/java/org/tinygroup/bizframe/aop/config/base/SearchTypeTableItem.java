/**
 *
 * Copyright (c) 2014-2017 All Rights Reserved.
 */
package org.tinygroup.bizframe.aop.config.base;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * 
 * @author zhangliang08072
 * @version $Id: SearchTypeTableMapping.java, v 0.1 2017-4-18 下午2:36:14 zhangliang08072 Exp $
 */
@XStreamAlias("type-table-item")
public class SearchTypeTableItem {

	//全文检索的type
	@XStreamAsAttribute
    @XStreamAlias("search-type")
    private String searchType;
	
	//持久化对象类型
    @XStreamAsAttribute
    @XStreamAlias("class-name")
    private String className;
	
	//表名称
	@XStreamAsAttribute
    @XStreamAlias("table-name")
    private String tableName;
    
	//主键列名称
    @XStreamAsAttribute
    @XStreamAlias("column-name")
    private String columnName;
    
    
    //主键是否是字符串类型
    @XStreamAsAttribute
    @XStreamAlias("is-id-string")
    private Boolean isIdString;


	public String getSearchType() {
		return searchType;
	}


	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}


	public String getClassName() {
		return className;
	}


	public void setClassName(String className) {
		this.className = className;
	}


	public String getTableName() {
		return tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getColumnName() {
		return columnName;
	}


	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}


	public Boolean getIsIdString() {
		return isIdString;
	}


	public void setIsIdString(Boolean isIdString) {
		this.isIdString = isIdString;
	}

}
