package org.tinygroup.bizframe.aop.config.base;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("extensions")
public class Extensions {
    @XStreamImplicit
    private List<Extension> extensionList;
    
    @XStreamImplicit
	private List<SearchTypeTableItem> searchTypeTableItems;

    public List<Extension> getExtensionList() {
        if (extensionList == null) {
            extensionList = new ArrayList<Extension>();
        }
        return extensionList;
    }

    public void setExtensionList(List<Extension> extensionList) {
        this.extensionList = extensionList;
    }

	public List<SearchTypeTableItem> getSearchTypeTableItems() {
		if(searchTypeTableItems == null) {
			searchTypeTableItems = new ArrayList<SearchTypeTableItem>();
		}
		return searchTypeTableItems;
	}

	public void setSearchTypeTableItems(
			List<SearchTypeTableItem> searchTypeTableItems) {
		this.searchTypeTableItems = searchTypeTableItems;
	}

}
