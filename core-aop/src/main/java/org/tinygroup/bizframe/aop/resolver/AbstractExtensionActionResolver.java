package org.tinygroup.bizframe.aop.resolver;

import org.springframework.beans.factory.InitializingBean;
import org.tinygroup.bizframe.aop.interceptor.ExtensionInterceptor;
import org.tinygroup.commons.tools.Assert;

public abstract class AbstractExtensionActionResolver implements ExtensionActionResolver, InitializingBean {
    private ExtensionInterceptor interceptor;
    private int order;

    public void setInterceptor(ExtensionInterceptor interceptor) {
        this.interceptor = interceptor;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.assertNotNull(interceptor, "FoundationExtensionInterceptor must not be null");
        interceptor.addActionResolver(this);
    }

    @Override
    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
