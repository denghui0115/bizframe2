package org.tinygroup.bizframe.aop.resolver;

import java.lang.reflect.Method;
import java.util.List;

import org.tinygroup.bizframe.aop.config.base.ExtensionAction;
import org.tinygroup.bizframe.aop.xmlfileparser.ExtensionActionXmlManager;


public class XmlExtensionActionResolver extends AbstractExtensionActionResolver {
    private ExtensionActionXmlManager extensionActionXmlManager;

    public void setExtensionActionXmlManager(ExtensionActionXmlManager extensionActionXmlManager) {
        this.extensionActionXmlManager = extensionActionXmlManager;
    }

    @Override
    public List<ExtensionAction> resolve(Method method) {
        return extensionActionXmlManager.getActionsByMethod(method);
    }
}
