/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50525
Source Host           : localhost:3306
Source Database       : bizframe2

Target Server Type    : MYSQL
Target Server Version : 50525
File Encoding         : 65001

Date: 2017-03-08 15:25:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tberrormsg`
-- ----------------------------
DROP TABLE IF EXISTS `tberrormsg`;
CREATE TABLE `tberrormsg` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `err_code` varchar(128) NOT NULL COMMENT '错误码',
  `err_msg` varchar(400) NOT NULL COMMENT '错误信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tberrormsg
-- ----------------------------

-- ----------------------------
-- Table structure for `tbhisuerpwd`
-- ----------------------------
DROP TABLE IF EXISTS `tbhisuerpwd`;
CREATE TABLE `tbhisuerpwd` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `serial_no` varchar(128) NOT NULL COMMENT '流水号',
  `user_id` varchar(128) NOT NULL COMMENT '用户代码',
  `user_pwd` varchar(128) NOT NULL COMMENT '用户密码',
  `reserve1` varchar(1000) DEFAULT NULL COMMENT '备用1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbhisuerpwd
-- ----------------------------

-- ----------------------------
-- Table structure for `tbparam`
-- ----------------------------
DROP TABLE IF EXISTS `tbparam`;
CREATE TABLE `tbparam` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `param_id` varchar(256) NOT NULL COMMENT 'PARAM_ID',
  `param_name` varchar(1000) NOT NULL COMMENT '参数标识名称',
  `param_value` varchar(1000) NOT NULL COMMENT '参数值',
  `value_name` varchar(1000) NOT NULL COMMENT '参数名称',
  `belong_type` varchar(4) NOT NULL COMMENT '归属系统',
  `modi_flag` varchar(4) NOT NULL COMMENT '允许修改标志',
  `reserve1` varchar(1000) DEFAULT NULL COMMENT '备用1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbparam
-- ----------------------------

-- ----------------------------
-- Table structure for `tbtrans`
-- ----------------------------
DROP TABLE IF EXISTS `tbtrans`;
CREATE TABLE `tbtrans` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `trans_code` varchar(32) NOT NULL COMMENT '交易编号',
  `trans_name` varchar(1000) NOT NULL COMMENT '交易名称',
  `enable_flag` varchar(1) NOT NULL COMMENT '交易允许',
  `channels` varchar(200) NOT NULL COMMENT '允许渠道组',
  `host_online` varchar(1) NOT NULL COMMENT '后台联机',
  `trans_type` varchar(1) NOT NULL COMMENT '交易类别',
  `monitor_status` varchar(1) NOT NULL COMMENT '监控标志',
  `log_level` varchar(1) NOT NULL COMMENT '日志级别',
  `mon_trans_type` varchar(1) NOT NULL COMMENT '监控交易类别',
  `reserve1` varchar(1000) DEFAULT NULL COMMENT '备用1',
  `reserve2` varchar(1000) DEFAULT NULL COMMENT '备注2',
  `reserve3` varchar(1000) DEFAULT NULL COMMENT '保留域3',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_trans` (`trans_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbtrans
-- ----------------------------

-- ----------------------------
-- Table structure for `tiny_metadata_resource`
-- ----------------------------
DROP TABLE IF EXISTS `tiny_metadata_resource`;
CREATE TABLE `tiny_metadata_resource` (
  `TYPE` varchar(20) DEFAULT NULL,
  `RESOURCE_ID` varchar(32) DEFAULT NULL,
  `MODIFIED_TIME` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tiny_metadata_resource
-- ----------------------------
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', 'a592c069796e457498f7e1cc71cf542e', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', '87744626d528433e878ffa8be128c3a8', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', '7e896e6d8fbb428492e88ca5bf3f2f24', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', '6975f3662c5f4f72a691ed14e1905f2d', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', 'a9c0b7c3aaf04fe6ac99e4a1735cfd80', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', '0b1cba14694b449ba640d330253090da', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', 'b1cdacb7c7164e9aa0db80e43ce81eff', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', 'c07a17ae55894ef28530f49516f01082', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', 'abc7e022ffe242b9a61d84cbe5ae36ef', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', 'cb2028c116be46a987e7e5c2a8103670', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', 'be35e68d661d454e9b2eebd064298122', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', 'f7793d8caf034299b6fcf3818aca1da2', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', '065570a0619d42e496a6f4575e045e64', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', '40473faca3514532946088d1d8619ce3', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', '447f0d4812e14ff08e8e87a1bb122840', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', '3803b3f2f4b04322ab82eed460f10130', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', '4e83355c928a4b8da413e9bb8bb8e11e', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', '7bd362e547954def8efdd9c138d74003', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('TABLE', '9b7d6a3df0b242a09d55e46dadbdb78c', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('INIT_DATA', '4b63a7ab841042b5b3c89971e066eb67', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('INIT_DATA', '00e5dc0c79374b16b17cf117689e2b8e', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('INIT_DATA', 'bc6154e97b0c4cf782eae41fede67e93', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('INIT_DATA', '9541361488ed47178779a10b3bbbbb25', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('INIT_DATA', '8a456faee7ed42cc9e878572e537d91b', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('INIT_DATA', 'c7b5af8b50b7406aabbdf5b7f647b3ab', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('INIT_DATA', '62c8767dd2834b2d868ac3324b71cb28', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('INIT_DATA', '60df4acb103b49a69d3e8a67f23edd02', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('INIT_DATA', 'd0b852d11ffc4d16ba35af9649dd7d36', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('INIT_DATA', 'bbf0fd9f9617455abb17925188efdccd', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('INIT_DATA', 'd2317b22ac854a618a25b8850cd94839', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('INIT_DATA', '8bfed3a8cedd4ee099cccc47abe73711', '1488857628000');
INSERT INTO `tiny_metadata_resource` VALUES ('INIT_DATA', '8d1279f1b18444a6adb3d4bf09cf34d6', '1488857628000');

-- ----------------------------
-- Table structure for `tsys_branch`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_branch`;
CREATE TABLE `tsys_branch` (
  `branch_code` varchar(64) NOT NULL COMMENT '机构编号',
  `branch_level` varchar(128) DEFAULT NULL COMMENT '机构级别',
  `branch_name` varchar(256) DEFAULT NULL COMMENT '机构名称',
  `branch_type` varchar(128) DEFAULT NULL COMMENT '机构类别',
  `short_name` varchar(128) DEFAULT NULL COMMENT '简称',
  `parent_code` varchar(64) DEFAULT NULL COMMENT '上级',
  `branch_path` varchar(1024) DEFAULT NULL COMMENT '机构内码',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `ext_field_1` varchar(1024) DEFAULT NULL COMMENT '扩展字段1',
  `ext_field_2` varchar(1024) DEFAULT NULL COMMENT '扩展字段2',
  `ext_field_3` varchar(1024) DEFAULT NULL COMMENT '扩展字段3',
  PRIMARY KEY (`branch_code`),
  KEY `fk_tsys_branch` (`parent_code`),
  CONSTRAINT `fk_tsys_branch` FOREIGN KEY (`parent_code`) REFERENCES `tsys_branch` (`branch_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_branch
-- ----------------------------
INSERT INTO `tsys_branch` VALUES ('09001', '-1', 'tiny', null, 'tiny', null, null, null, null, null, null);
INSERT INTO `tsys_branch` VALUES ('09090', '0', 'tiny测试', null, 'tiny测试', '09001', null, null, null, null, null);
INSERT INTO `tsys_branch` VALUES ('09092', '1', 'tiny开发', null, 'tiny开发', '09092', null, null, null, null, null);

-- ----------------------------
-- Table structure for `tsys_branch_user`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_branch_user`;
CREATE TABLE `tsys_branch_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` varchar(128) NOT NULL COMMENT '用户代码',
  `branch_code` varchar(64) NOT NULL COMMENT '机构编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_branch_user
-- ----------------------------

-- ----------------------------
-- Table structure for `tsys_dep`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_dep`;
CREATE TABLE `tsys_dep` (
  `dep_code` varchar(64) NOT NULL COMMENT '部门编号',
  `dep_name` varchar(256) DEFAULT NULL COMMENT '部门名称',
  `short_name` varchar(128) DEFAULT NULL COMMENT '简称',
  `parent_code` varchar(64) DEFAULT NULL COMMENT '上级',
  `branch_code` varchar(64) NOT NULL COMMENT '机构编号',
  `dep_path` varchar(1024) DEFAULT NULL COMMENT '部门内码',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `ext_field_1` varchar(1024) DEFAULT NULL COMMENT '扩展字段1',
  `ext_field_2` varchar(1024) DEFAULT NULL COMMENT '扩展字段2',
  `ext_field_3` varchar(1024) DEFAULT NULL COMMENT '扩展字段3',
  PRIMARY KEY (`dep_code`),
  KEY `fk_tsys_dep` (`parent_code`),
  CONSTRAINT `fk_tsys_dep` FOREIGN KEY (`parent_code`) REFERENCES `tsys_dep` (`dep_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_dep
-- ----------------------------
INSERT INTO `tsys_dep` VALUES ('090001', 'java组', 'java组', null, '09092', null, null, null, null, null);
INSERT INTO `tsys_dep` VALUES ('090002', 'tinyui组', 'tinyui组', null, '09092', null, null, null, null, null);
INSERT INTO `tsys_dep` VALUES ('db', 'db', 'db', '090001', '09092', null, null, null, null, null);
INSERT INTO `tsys_dep` VALUES ('ext', 'ext', 'ext', '090001', '09092', null, null, null, null, null);
INSERT INTO `tsys_dep` VALUES ('flow', 'flow', 'flow', '090001', '09092', null, null, null, null, null);
INSERT INTO `tsys_dep` VALUES ('framework', 'framework', 'framework', '090001', '09092', null, null, null, null, null);

-- ----------------------------
-- Table structure for `tsys_dict_entry`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_dict_entry`;
CREATE TABLE `tsys_dict_entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dict_entry_code` varchar(128) NOT NULL COMMENT '字典条目编号',
  `kind_code` varchar(64) NOT NULL COMMENT '分类编号',
  `dict_entry_name` varchar(128) NOT NULL COMMENT '字典条目名称',
  `ctrl_flag` varchar(32) DEFAULT NULL COMMENT '控制标志',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dict_entry_code` (`dict_entry_code`),
  KEY `fk_dictentry_kind` (`kind_code`),
  CONSTRAINT `fk_dictentry_kind` FOREIGN KEY (`kind_code`) REFERENCES `tsys_kind` (`kind_code`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_dict_entry
-- ----------------------------
INSERT INTO `tsys_dict_entry` VALUES ('2', 'BIZ_RIGHT_FLAG', 'BIZFRAME', '授权标志', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('3', 'BIZ_SUB_SYSTEM', 'BIZFRAME', '子系统', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('4', 'BIZ_MODEL', 'BIZFRAME', '模块', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('5', 'BIZ_LOGIN_FLAG', 'BIZFRAME', '登录标志', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('6', 'BIZ_SYN_AUTH', 'BIZFRAME', '同步授权', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('7', 'BIZ_DICT_TYPE', 'BIZFRAME', '数据字典类型', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('8', 'SYS_BRANCH_LEVEL', 'BIZFRAME', '机构级别', null, '机构级别');
INSERT INTO `tsys_dict_entry` VALUES ('9', 'BIZ_WINDOW_CATE', 'BIZFRAME', '窗口类型', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('10', 'BIZ_RIGHT_CATE', 'BIZFRAME', '权限分类', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('11', 'BIZ_SERV_CATE', 'BIZFRAME', '服务分类', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('12', 'BIZ_ORG_DIMEN', 'BIZFRAME', '组织维度', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('13', 'BIZ_ORG_CATE', 'BIZFRAME', '组织分类', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('14', 'BIZ_ORG_LEVEL', 'BIZFRAME', '组织级别', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('15', 'BIZ_USER_CATE', 'BIZFRAME', '用户分类', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('16', 'BIZ_USER_STATUS', 'BIZFRAME', '用户状态', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('18', 'BIZ_YES_OR_NO', 'BIZFRAME', '是否标志', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('19', 'BIZ_TIME_CATE', 'BIZFRAME', '时间单位', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('21', 'BIZ_DB_TYPE', 'BIZFRAME', '数据库分类', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('22', 'FIN_DICT_FLAG', 'BIZFRAME', '条目标志', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('23', 'BIZ_USER_GROUP', 'BIZFRAME', '用户组别', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('24', 'BIZ_KIND_DIMEN', 'BIZFRAME', '分类维度', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('25', 'BIZ_LOCK_STATUS', 'BIZFRAME', '锁定状态', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('27', 'BIZ_CONTROL_FLAG', 'BIZFRAME', '控制标志', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('28', 'BIZ_READ_OR_WRITE', 'BIZFRAME', '读写属性', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('29', 'BIZ_EXPAND_FLAG', 'BIZFRAME', '展开标志', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('31', 'K_YSCLBZ', 'BIZFRAME', '延时处理', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('32', 'K_ZYGSMK', 'BIZFRAME', '模块类型', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('33', 'K_ZYGSYM', 'BIZFRAME', '归属页面页码', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('34', 'K_PCLZT', 'BIZFRAME', '批处理状态', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('35', 'K_JYQD', 'BIZFRAME', '允许渠道组', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('36', 'K_RZJB', 'BIZFRAME', '日志级别', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('37', 'K_JYLB', 'BIZFRAME', '交易类别', null, null);
INSERT INTO `tsys_dict_entry` VALUES ('38', 'K_GSXT', 'BIZFRAME', '归属系统', null, '归属系统');
INSERT INTO `tsys_dict_entry` VALUES ('39', 'BIZ_MENU_TYPE', 'BIZFRAME', '菜单类型', null, null);

-- ----------------------------
-- Table structure for `tsys_dict_item`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_dict_item`;
CREATE TABLE `tsys_dict_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dict_item_code` varchar(1024) DEFAULT NULL COMMENT '字典项编号',
  `dict_item_name` varchar(240) DEFAULT NULL COMMENT '字典项名称',
  `dict_item_order` int(11) DEFAULT NULL COMMENT '字典项排序',
  `dict_entry_id` int(11) DEFAULT NULL COMMENT '字典条目ID',
  PRIMARY KEY (`id`),
  KEY `fk_dict_entry_item` (`dict_entry_id`),
  CONSTRAINT `fk_dict_entry_item` FOREIGN KEY (`dict_entry_id`) REFERENCES `tsys_dict_entry` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_dict_item
-- ----------------------------
INSERT INTO `tsys_dict_item` VALUES ('1', '1', '操作', null, '2');
INSERT INTO `tsys_dict_item` VALUES ('2', '2', '授权', null, '2');
INSERT INTO `tsys_dict_item` VALUES ('4', '1', '系统模块', null, '4');
INSERT INTO `tsys_dict_item` VALUES ('5', '2', '用户模块', null, '4');
INSERT INTO `tsys_dict_item` VALUES ('6', '3', '系统管理', null, '4');
INSERT INTO `tsys_dict_item` VALUES ('7', '4', '日常运维', null, '4');
INSERT INTO `tsys_dict_item` VALUES ('8', '5', '产品管理', null, '4');
INSERT INTO `tsys_dict_item` VALUES ('9', '6', '辅助管理', null, '4');
INSERT INTO `tsys_dict_item` VALUES ('10', '7', '查询管理', null, '4');
INSERT INTO `tsys_dict_item` VALUES ('11', '8', '打印管理', null, '4');
INSERT INTO `tsys_dict_item` VALUES ('12', '9', '公共交易', null, '4');
INSERT INTO `tsys_dict_item` VALUES ('13', 'a', '基金业务', null, '4');
INSERT INTO `tsys_dict_item` VALUES ('14', 'b', '行内理财', null, '4');
INSERT INTO `tsys_dict_item` VALUES ('15', 'c', '基金业务1.2', null, '4');
INSERT INTO `tsys_dict_item` VALUES ('16', '1', '登录', null, '5');
INSERT INTO `tsys_dict_item` VALUES ('17', '0', '无', null, '5');
INSERT INTO `tsys_dict_item` VALUES ('18', '-1', '集团', null, '8');
INSERT INTO `tsys_dict_item` VALUES ('19', '0', '总公司', null, '8');
INSERT INTO `tsys_dict_item` VALUES ('20', '1', '子公司', null, '8');
INSERT INTO `tsys_dict_item` VALUES ('21', '0', '子窗口', null, '9');
INSERT INTO `tsys_dict_item` VALUES ('22', '1', '弹出窗口', null, '9');
INSERT INTO `tsys_dict_item` VALUES ('23', '0', '总部', null, '14');
INSERT INTO `tsys_dict_item` VALUES ('24', '1', '事业部', null, '14');
INSERT INTO `tsys_dict_item` VALUES ('25', '2', '产品部', null, '14');
INSERT INTO `tsys_dict_item` VALUES ('26', '1', '员工', null, '15');
INSERT INTO `tsys_dict_item` VALUES ('27', '0', '用户', null, '15');
INSERT INTO `tsys_dict_item` VALUES ('28', '1', '注销', null, '16');
INSERT INTO `tsys_dict_item` VALUES ('29', '2', '禁用', null, '16');
INSERT INTO `tsys_dict_item` VALUES ('30', '0', '签退', null, '16');
INSERT INTO `tsys_dict_item` VALUES ('31', '1', '登录', null, '16');
INSERT INTO `tsys_dict_item` VALUES ('32', '0', '操作组', null, '23');
INSERT INTO `tsys_dict_item` VALUES ('33', '1', '普通组', null, '23');
INSERT INTO `tsys_dict_item` VALUES ('34', '2', '授权组', null, '23');
INSERT INTO `tsys_dict_item` VALUES ('35', '0', '数据字典', null, '24');
INSERT INTO `tsys_dict_item` VALUES ('36', '1', '系统参数', null, '24');
INSERT INTO `tsys_dict_item` VALUES ('37', '2', '标准字段', null, '24');
INSERT INTO `tsys_dict_item` VALUES ('38', '3', '系统资源', null, '24');
INSERT INTO `tsys_dict_item` VALUES ('39', '4', '系统菜单', null, '24');
INSERT INTO `tsys_dict_item` VALUES ('40', '5', '子系统', null, '24');
INSERT INTO `tsys_dict_item` VALUES ('41', '0', '签退', null, '25');
INSERT INTO `tsys_dict_item` VALUES ('42', '1', '登录', null, '25');
INSERT INTO `tsys_dict_item` VALUES ('43', '2', '非正常签退', null, '25');
INSERT INTO `tsys_dict_item` VALUES ('44', '3', '锁定', null, '25');
INSERT INTO `tsys_dict_item` VALUES ('45', '4', '密码锁定', null, '25');
INSERT INTO `tsys_dict_item` VALUES ('46', '0', '不控制', null, '27');
INSERT INTO `tsys_dict_item` VALUES ('47', '1', '控制', null, '27');
INSERT INTO `tsys_dict_item` VALUES ('48', '0', '读', null, '28');
INSERT INTO `tsys_dict_item` VALUES ('49', '1', '写', null, '28');
INSERT INTO `tsys_dict_item` VALUES ('50', '0', '展开', null, '29');
INSERT INTO `tsys_dict_item` VALUES ('51', '1', '不展开', null, '29');
INSERT INTO `tsys_dict_item` VALUES ('52', '0', '无', null, '31');
INSERT INTO `tsys_dict_item` VALUES ('53', '1', '延时', null, '31');
INSERT INTO `tsys_dict_item` VALUES ('54', '2', '定时', null, '31');
INSERT INTO `tsys_dict_item` VALUES ('55', '3', '中断', null, '31');
INSERT INTO `tsys_dict_item` VALUES ('56', '0', '未激活', null, '34');
INSERT INTO `tsys_dict_item` VALUES ('57', '1', '已激活', null, '34');
INSERT INTO `tsys_dict_item` VALUES ('58', '2', '作业暂停', null, '34');
INSERT INTO `tsys_dict_item` VALUES ('59', '3', '作业完成', null, '34');
INSERT INTO `tsys_dict_item` VALUES ('60', '4', '作业失败', null, '34');
INSERT INTO `tsys_dict_item` VALUES ('61', '5', '作业中断', null, '34');
INSERT INTO `tsys_dict_item` VALUES ('62', 'Z', '正在处理', null, '34');
INSERT INTO `tsys_dict_item` VALUES ('63', '9', '批量发起', null, '35');
INSERT INTO `tsys_dict_item` VALUES ('64', 'G', 'WEB管理台', null, '35');
INSERT INTO `tsys_dict_item` VALUES ('65', '0', 'TRACE', null, '36');
INSERT INTO `tsys_dict_item` VALUES ('66', '1', 'DEBUG', null, '36');
INSERT INTO `tsys_dict_item` VALUES ('67', '2', 'INFO', null, '36');
INSERT INTO `tsys_dict_item` VALUES ('68', '3', 'WARN', null, '36');
INSERT INTO `tsys_dict_item` VALUES ('69', '4', 'ERROR', null, '36');
INSERT INTO `tsys_dict_item` VALUES ('70', '5', 'FATAL', null, '36');
INSERT INTO `tsys_dict_item` VALUES ('71', '6', 'NOLOG', null, '36');
INSERT INTO `tsys_dict_item` VALUES ('72', '0', '账户类', null, '37');
INSERT INTO `tsys_dict_item` VALUES ('73', '1', '交易类', null, '37');
INSERT INTO `tsys_dict_item` VALUES ('74', '2', '撤销', null, '37');
INSERT INTO `tsys_dict_item` VALUES ('76', '3', '管理台', null, '37');
INSERT INTO `tsys_dict_item` VALUES ('77', '4', '查询类', null, '37');
INSERT INTO `tsys_dict_item` VALUES ('78', '0', '销售', null, '38');
INSERT INTO `tsys_dict_item` VALUES ('79', 'Z', '公共', null, '38');
INSERT INTO `tsys_dict_item` VALUES ('80', '0', '菜单', null, '39');
INSERT INTO `tsys_dict_item` VALUES ('81', '1', '按钮', null, '39');

-- ----------------------------
-- Table structure for `tsys_kind`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_kind`;
CREATE TABLE `tsys_kind` (
  `kind_code` varchar(64) NOT NULL COMMENT '分类编号',
  `kind_type` varchar(32) NOT NULL COMMENT '分类类型',
  `kind_name` varchar(128) NOT NULL COMMENT '分类名称',
  `parent_code` varchar(64) DEFAULT NULL COMMENT '上级',
  `mnemonic` varchar(64) NOT NULL COMMENT '助记符',
  `tree_idx` varchar(1024) DEFAULT NULL COMMENT '树索引码',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`kind_code`),
  KEY `fk_tsys_kind` (`parent_code`),
  CONSTRAINT `fk_tsys_kind` FOREIGN KEY (`parent_code`) REFERENCES `tsys_kind` (`kind_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_kind
-- ----------------------------
INSERT INTO `tsys_kind` VALUES ('BIZFRAME', '0', 'BIZFRAME', null, 'BIZFRAME', null, 'BIZFRAME');

-- ----------------------------
-- Table structure for `tsys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_menu`;
CREATE TABLE `tsys_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `menu_code` varchar(128) NOT NULL COMMENT '菜单编号',
  `kind_code` varchar(64) NOT NULL COMMENT '分类编号',
  `menu_name` varchar(128) NOT NULL COMMENT '菜单名称',
  `menu_arg` varchar(1024) DEFAULT NULL COMMENT '菜单参数',
  `menu_icon` varchar(1024) DEFAULT NULL COMMENT '菜单图标',
  `window_type` varchar(32) DEFAULT NULL COMMENT '窗口类型',
  `tip` varchar(1024) DEFAULT NULL COMMENT '提示信息',
  `hot_key` varchar(32) DEFAULT NULL COMMENT '快捷键',
  `parent_id` int(11) DEFAULT NULL COMMENT 'PARENT_ID',
  `order_no` int(11) DEFAULT NULL COMMENT '序号',
  `open_flag` varchar(32) DEFAULT NULL COMMENT '展开标志',
  `tree_idx` varchar(1024) DEFAULT NULL COMMENT '树索引码',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `window_model` varchar(32) DEFAULT NULL COMMENT '窗口模式',
  `menu_url` varchar(1020) DEFAULT NULL COMMENT '菜单URL',
  `menu_type` varchar(32) NOT NULL COMMENT '菜单类型 0,表示菜单。1，表示按钮',
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `IDX_TSYS_MENU` (`menu_code`,`kind_code`),
  KEY `fk_sysmenu_kind` (`kind_code`),
  KEY `fk_menu_parent` (`parent_id`),
  CONSTRAINT `fk_menu_parent` FOREIGN KEY (`parent_id`) REFERENCES `tsys_menu` (`menu_id`),
  CONSTRAINT `fk_sysmenu_kind` FOREIGN KEY (`kind_code`) REFERENCES `tsys_kind` (`kind_code`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_menu
-- ----------------------------
INSERT INTO `tsys_menu` VALUES ('1', 'm001', 'BIZFRAME', '系统管理', 'cs', 'cogs', '!', 'xx', null, null, null, null, null, null, null, '/sysdictentry/manage', '0');
INSERT INTO `tsys_menu` VALUES ('20', 'm002', 'BIZFRAME', '基础参数管理', null, 'fa-sitemap', '!', null, null, '1', null, null, null, null, null, '/sysdictentry/manage.page', '0');
INSERT INTO `tsys_menu` VALUES ('21', 'm003', 'BIZFRAME', '用户管理', null, 'fa-user', '!', null, null, '1', null, null, null, null, null, 'sysuser/manage', '0');
INSERT INTO `tsys_menu` VALUES ('24', 'm004', 'BIZFRAME', '数据字典设置', null, null, '0', null, null, '20', null, null, null, null, null, '/sysdictentry/manage.page', '0');
INSERT INTO `tsys_menu` VALUES ('25', 'm005', 'BIZFRAME', '系统菜单设置', null, null, null, null, null, '20', null, null, null, null, null, '/sysmenu/manage.page', '0');
INSERT INTO `tsys_menu` VALUES ('27', 'm006', 'BIZFRAME', '岗位设置', null, null, '!', null, null, '21', null, null, null, null, null, '/sysoffice/manage.page', '0');
INSERT INTO `tsys_menu` VALUES ('28', 'm007', 'BIZFRAME', '角色设置', null, null, '!', null, null, '21', null, null, null, null, null, '/sysrole/manage', '0');
INSERT INTO `tsys_menu` VALUES ('29', 'm008', 'BIZFRAME', '用户设置', null, null, '!', null, null, '21', null, null, null, null, null, '/sysuser/manage', '0');
INSERT INTO `tsys_menu` VALUES ('30', 'm009', 'BIZFRAME', '部门设置', null, null, null, null, null, '21', null, null, null, null, null, '/sysdep/manage.page', '0');
INSERT INTO `tsys_menu` VALUES ('33', 'm010', 'BIZFRAME', '机构设置', null, null, null, null, null, '21', null, null, null, null, null, '/sysbranch/manage.page', '0');
INSERT INTO `tsys_menu` VALUES ('37', 'm011', 'BIZFRAME', '系统类别设置', null, null, null, null, null, '21', null, null, null, null, null, '/syskind/manage.page', '0');
INSERT INTO `tsys_menu` VALUES ('41', 'm012', 'BIZFRAME', '交易设置', null, null, null, null, null, '21', null, null, null, null, null, '/sysbiztrans/manage', '0');
INSERT INTO `tsys_menu` VALUES ('42', 'm013', 'BIZFRAME', '错误代码设置', null, null, null, null, null, '20', null, null, null, null, null, '/syserrorcode/manage', '0');
INSERT INTO `tsys_menu` VALUES ('46', 'm014', 'BIZFRAME', '主页设置', null, null, null, null, null, '21', null, null, null, null, null, '/sysuser/home/manage', '0');
INSERT INTO `tsys_menu` VALUES ('48', 'm015', 'BIZFRAME', '参数设置', null, null, null, null, null, '20', null, null, null, null, null, '/sysparam/manage', '0');
INSERT INTO `tsys_menu` VALUES ('52', 'b001', 'BIZFRAME', '菜单新增按钮', '', '', '', '', '', '25', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('53', 'b002', 'BIZFRAME', '菜单编辑按钮', '', '', '', '', '', '25', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('54', 'b003', 'BIZFRAME', '菜单删除按钮', '', '', '', '', '', '25', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('55', 'b004', 'BIZFRAME', '菜单查询按钮', '', '', '', '', '', '25', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('56', 'b005', 'BIZFRAME', '数据字典新增按钮', '', '', '', '', '', '24', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('57', 'b006', 'BIZFRAME', '数据字典编辑按钮', '', '', '', '', '', '24', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('58', 'b007', 'BIZFRAME', '数据字典删除按钮', '', '', '', '', '', '24', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('59', 'b008', 'BIZFRAME', '数据字典详细按钮', '', '', '', '', '', '24', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('60', 'b009', 'BIZFRAME', '数据字典查询按钮', '', '', '', '', '', '24', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('61', 'b010', 'BIZFRAME', '错误代码新增按钮', '', '', '', '', '', '42', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('62', 'b011', 'BIZFRAME', '错误代码编辑按钮', '', '', '', '', '', '42', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('63', 'b012', 'BIZFRAME', '错误代码删除按钮', '', '', '', '', '', '42', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('64', 'b013', 'BIZFRAME', '错误代码查询按钮', '', '', '', '', '', '42', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('65', 'b014', 'BIZFRAME', '参数设置新增按钮', '', '', '', '', '', '48', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('66', 'b015', 'BIZFRAME', '参数设置编辑按钮', '', '', '', '', '', '48', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('67', 'b016', 'BIZFRAME', '参数设置删除按钮', '', '', '', '', '', '48', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('68', 'b017', 'BIZFRAME', '参数设置查询按钮', '', '', '', '', '', '48', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('69', 'b018', 'BIZFRAME', '交易设置新增按钮', '', '', '', '', '', '41', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('70', 'b019', 'BIZFRAME', '交易设置编辑按钮', '', '', '', '', '', '41', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('71', 'b020', 'BIZFRAME', '交易设置删除按钮', '', '', '', '', '', '41', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('72', 'b021', 'BIZFRAME', '交易设置查询按钮', '', '', '', '', '', '41', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('73', 'b022', 'BIZFRAME', '角色新增按钮', '', '', '', '', '', '28', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('74', 'b023', 'BIZFRAME', '角色编辑按钮', '', '', '', '', '', '28', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('75', 'b024', 'BIZFRAME', '角色授权按钮', '', '', '', '', '', '28', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('76', 'b025', 'BIZFRAME', '角色删除按钮', '', '', '', '', '', '28', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('77', 'b026', 'BIZFRAME', '角色查询按钮', '', '', '', '', '', '28', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('78', 'b027', 'BIZFRAME', '用户新增按钮', '', '', '', '', '', '29', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('79', 'b028', 'BIZFRAME', '用户删除按钮', '', '', '', '', '', '29', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('80', 'b029', 'BIZFRAME', '用户锁定按钮', '', '', '', '', '', '29', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('81', 'b030', 'BIZFRAME', '用户解锁按钮', '', '', '', '', '', '29', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('82', 'b031', 'BIZFRAME', '用户密码重置按钮', '', '', '', '', '', '29', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('83', 'b032', 'BIZFRAME', '用户详情按钮', '', '', '', '', '', '29', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('84', 'b033', 'BIZFRAME', '用户编辑按钮', '', '', '', '', '', '29', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('85', 'b034', 'BIZFRAME', '用户角色分配按钮', '', '', '', '', '', '29', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('86', 'b035', 'BIZFRAME', '用户权限分配按钮', '', '', '', '', '', '29', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('87', 'b036', 'BIZFRAME', '用户岗位分配按钮', '', '', '', '', '', '29', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('88', 'b037', 'BIZFRAME', '用户查询按钮', '', '', '', '', '', '29', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('89', 'b038', 'BIZFRAME', '部门设置新增按钮', '', '', '', '', '', '30', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('90', 'b039', 'BIZFRAME', '部门设置删除按钮', '', '', '', '', '', '30', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('91', 'b040', 'BIZFRAME', '部门设置编辑按钮', '', '', '', '', '', '30', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('92', 'b041', 'BIZFRAME', '部门设置查询按钮', '', '', '', '', '', '30', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('93', 'b042', 'BIZFRAME', '机构设置新增按钮', '', '', '', '', '', '33', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('94', 'b043', 'BIZFRAME', '机构设置删除按钮', '', '', '', '', '', '33', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('95', 'b044', 'BIZFRAME', '机构设置编辑按钮', '', '', '', '', '', '33', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('96', 'b045', 'BIZFRAME', '机构设置查询按钮', '', '', '', '', '', '33', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('97', 'b046', 'BIZFRAME', '系统类别设置新增按钮', '', '', '', '', '', '37', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('98', 'b047', 'BIZFRAME', '系统类别设置删除按钮', '', '', '', '', '', '37', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('99', 'b048', 'BIZFRAME', '系统类别设置编辑按钮', '', '', '', '', '', '37', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('100', 'b049', 'BIZFRAME', '系统类别设置查询按钮', '', '', '', '', '', '37', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('101', 'b050', 'BIZFRAME', '主页设置编辑按钮', '', '', '', '', '', '46', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('102', 'b051', 'BIZFRAME', '主页设置查询按钮', '', '', '', '', '', '46', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('103', 'b052', 'BIZFRAME', '岗位设置新增按钮', '', '', '', '', '', '27', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('104', 'b053', 'BIZFRAME', '岗位设置删除按钮', '', '', '', '', '', '27', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('105', 'b054', 'BIZFRAME', '岗位设置编辑按钮', '', '', '', '', '', '27', null, '', null, '', null, '', '1');
INSERT INTO `tsys_menu` VALUES ('106', 'b055', 'BIZFRAME', '岗位设置查询按钮', '', '', '', '', '', '27', null, '', null, '', null, '', '1');

-- ----------------------------
-- Table structure for `tsys_office`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_office`;
CREATE TABLE `tsys_office` (
  `office_code` varchar(64) NOT NULL COMMENT '岗位编号',
  `office_name` varchar(256) DEFAULT NULL COMMENT '岗位名称',
  `short_name` varchar(128) DEFAULT NULL COMMENT '简称',
  `parent_code` varchar(64) DEFAULT NULL COMMENT '上级',
  `branch_code` varchar(64) NOT NULL COMMENT '机构编号',
  `dep_code` varchar(64) NOT NULL COMMENT '部门编号',
  `office_path` varchar(1024) DEFAULT NULL COMMENT '岗位内码',
  `branch_level` varchar(128) DEFAULT NULL COMMENT '机构级别',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `ext_field_1` varchar(1024) DEFAULT NULL COMMENT '扩展字段1',
  `ext_field_2` varchar(1024) DEFAULT NULL COMMENT '扩展字段2',
  `ext_field_3` varchar(1024) DEFAULT NULL COMMENT '扩展字段3',
  PRIMARY KEY (`office_code`),
  KEY `fk_tsys_office` (`parent_code`),
  CONSTRAINT `fk_tsys_office` FOREIGN KEY (`parent_code`) REFERENCES `tsys_office` (`office_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_office
-- ----------------------------
INSERT INTO `tsys_office` VALUES ('091', '高级工程师', '高级工程师', '909', '09090', '090002', null, null, null, null, null, null);
INSERT INTO `tsys_office` VALUES ('909', '技术专家', '技术专家', null, '09090', '090001', null, null, null, null, null, null);
INSERT INTO `tsys_office` VALUES ('j001', 'UI专家', 'UI专家', null, 'b002', '090002', null, null, null, null, null, null);
INSERT INTO `tsys_office` VALUES ('j002', 'UI工程师', 'UI2', 'j001', 'b002', '090002', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `tsys_office_user`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_office_user`;
CREATE TABLE `tsys_office_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` varchar(128) NOT NULL COMMENT '用户代码',
  `office_code` varchar(64) NOT NULL COMMENT '岗位编号',
  PRIMARY KEY (`id`),
  KEY `fk_office_user` (`office_code`),
  CONSTRAINT `fk_office_user` FOREIGN KEY (`office_code`) REFERENCES `tsys_office` (`office_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_office_user
-- ----------------------------

-- ----------------------------
-- Table structure for `tsys_role`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_role`;
CREATE TABLE `tsys_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_code` varchar(64) NOT NULL COMMENT '角色编号',
  `role_name` varchar(128) DEFAULT NULL COMMENT '角色名称',
  `creator` varchar(128) DEFAULT NULL COMMENT '创建者',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `IDX_TSYS_ROLE_ROLE_CODE` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_role
-- ----------------------------
INSERT INTO `tsys_role` VALUES ('1', 'r000', '系统管理', null, '系统管理');

-- ----------------------------
-- Table structure for `tsys_role_right`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_role_right`;
CREATE TABLE `tsys_role_right` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `menu_id` int(11) NOT NULL COMMENT '菜单id',
  `create_by` varchar(128) DEFAULT NULL COMMENT '分配人',
  `create_date` date DEFAULT NULL COMMENT '创建时间',
  `begin_date` int(11) DEFAULT NULL COMMENT '开始时间',
  `end_date` int(11) DEFAULT NULL COMMENT '截止时间',
  `right_flag` varchar(32) DEFAULT NULL COMMENT '授权标志',
  PRIMARY KEY (`id`),
  KEY `fk_r_right_role` (`role_id`),
  KEY `fk_r_right_menu` (`menu_id`),
  CONSTRAINT `fk_r_right_menu` FOREIGN KEY (`menu_id`) REFERENCES `tsys_menu` (`menu_id`),
  CONSTRAINT `fk_r_right_role` FOREIGN KEY (`role_id`) REFERENCES `tsys_role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_role_right
-- ----------------------------
INSERT INTO `tsys_role_right` VALUES ('1', '1', '1', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('2', '1', '20', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('3', '1', '21', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('4', '1', '24', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('5', '1', '25', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('6', '1', '29', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('7', '1', '30', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('8', '1', '33', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('9', '1', '37', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('10', '1', '41', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('11', '1', '42', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('12', '1', '46', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('13', '1', '48', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('14', '1', '27', null, null, null, null, null);
INSERT INTO `tsys_role_right` VALUES ('15', '1', '1', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('18', '1', '52', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('19', '1', '53', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('22', '1', '55', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('23', '1', '54', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('24', '1', '56', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('25', '1', '57', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('26', '1', '58', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('27', '1', '59', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('28', '1', '60', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('29', '1', '61', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('30', '1', '62', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('31', '1', '63', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('32', '1', '64', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('33', '1', '65', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('34', '1', '66', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('35', '1', '67', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('36', '1', '68', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('37', '1', '69', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('38', '1', '70', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('39', '1', '71', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('40', '1', '72', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('41', '1', '73', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('42', '1', '74', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('43', '1', '76', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('44', '1', '77', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('45', '1', '75', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('46', '1', '78', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('47', '1', '79', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('48', '1', '80', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('49', '1', '81', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('50', '1', '82', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('51', '1', '83', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('52', '1', '84', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('53', '1', '85', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('54', '1', '86', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('55', '1', '87', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('56', '1', '88', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('57', '1', '89', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('58', '1', '90', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('59', '1', '91', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('60', '1', '92', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('61', '1', '93', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('62', '1', '94', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('63', '1', '95', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('64', '1', '96', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('65', '1', '97', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('66', '1', '98', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('67', '1', '99', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('68', '1', '100', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('69', '1', '101', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('70', '1', '102', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('71', '1', '103', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('72', '1', '104', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('73', '1', '105', null, null, null, null, '1');
INSERT INTO `tsys_role_right` VALUES ('74', '1', '106', null, null, null, null, '1');

-- ----------------------------
-- Table structure for `tsys_role_user`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_role_user`;
CREATE TABLE `tsys_role_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_code` varchar(128) NOT NULL COMMENT '用户代码',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `right_flag` varchar(32) NOT NULL COMMENT '授权标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_role_user
-- ----------------------------
INSERT INTO `tsys_role_user` VALUES ('1', 'admin', '1', '1');

-- ----------------------------
-- Table structure for `tsys_user`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_user`;
CREATE TABLE `tsys_user` (
  `user_id` varchar(128) NOT NULL COMMENT '用户代码',
  `branch_code` varchar(64) DEFAULT NULL COMMENT '机构编号',
  `dep_code` varchar(64) DEFAULT NULL COMMENT '部门编号',
  `user_name` varchar(128) NOT NULL COMMENT '用户名称',
  `user_pwd` varchar(128) NOT NULL COMMENT '用户密码',
  `user_type` varchar(32) NOT NULL COMMENT '用户分类',
  `user_level` varchar(128) DEFAULT NULL COMMENT '用户组别',
  `user_status` varchar(32) NOT NULL COMMENT '用户状态',
  `lock_status` varchar(32) NOT NULL COMMENT '锁定状态',
  `creator` varchar(128) DEFAULT NULL COMMENT '创建者',
  `create_date` date DEFAULT NULL COMMENT '创建时间',
  `modify_date` date DEFAULT NULL COMMENT '最后修改时间',
  `pass_modify_date` date DEFAULT NULL COMMENT '密码修改时间',
  `auth_level` varchar(128) DEFAULT NULL COMMENT '授权级别',
  `home_page` varchar(128) DEFAULT NULL COMMENT '个性化主页',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `ext_field_1` varchar(1024) DEFAULT NULL COMMENT '扩展字段1',
  `ext_field_2` varchar(1024) DEFAULT NULL COMMENT '扩展字段2',
  `ext_field_4` varchar(1024) DEFAULT NULL COMMENT '扩展字段4',
  `ext_field_5` varchar(1024) DEFAULT NULL COMMENT '扩展字段5',
  `ext_field_3` varchar(1024) DEFAULT NULL COMMENT '扩展字段3',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_user
-- ----------------------------
INSERT INTO `tsys_user` VALUES ('admin', '09090', '090001', '超级管理员', 'e80b5017098950fc58aad83c8c14978e', '0', '0', '0', '1', null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `tsys_user_login`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_user_login`;
CREATE TABLE `tsys_user_login` (
  `user_id` varchar(128) NOT NULL COMMENT '用户代码',
  `last_login_date` date DEFAULT NULL COMMENT '上次成功登录日',
  `last_login_time` date DEFAULT NULL COMMENT '上次成功登录时',
  `last_login_ip` varchar(64) DEFAULT NULL COMMENT '最近登录操作IP',
  `login_fail_times` int(11) DEFAULT NULL COMMENT '登录累计失败次数',
  `last_fail_date` date DEFAULT NULL COMMENT '最后登录失败日',
  `ext_field` varchar(1024) DEFAULT NULL COMMENT '扩展字段',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_user_login
-- ----------------------------

-- ----------------------------
-- Table structure for `tsys_user_right`
-- ----------------------------
DROP TABLE IF EXISTS `tsys_user_right`;
CREATE TABLE `tsys_user_right` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` varchar(128) NOT NULL COMMENT '用户代码',
  `menu_id` int(11) NOT NULL COMMENT '菜单id',
  `create_by` varchar(128) DEFAULT NULL COMMENT '分配人',
  `create_date` date DEFAULT NULL COMMENT '创建时间',
  `begin_date` int(11) DEFAULT NULL COMMENT '开始时间',
  `end_date` int(11) DEFAULT NULL COMMENT '截止时间',
  `right_flag` varchar(32) DEFAULT NULL COMMENT '授权标志',
  PRIMARY KEY (`id`),
  KEY `fk_u_right_user` (`user_id`),
  KEY `fk_u_right_menu` (`menu_id`),
  CONSTRAINT `fk_u_right_menu` FOREIGN KEY (`menu_id`) REFERENCES `tsys_menu` (`menu_id`),
  CONSTRAINT `fk_u_right_user` FOREIGN KEY (`user_id`) REFERENCES `tsys_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tsys_user_right
-- ----------------------------
INSERT INTO `tsys_user_right` VALUES ('1', 'admin', '41', null, null, null, null, null);
INSERT INTO `tsys_user_right` VALUES ('2', 'admin', '28', null, null, null, null, null);
