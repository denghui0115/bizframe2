package org.tinygroup.bizframe.dictload;


import org.tinygroup.bizframe.business.inter.DictDataLoaderBusiness;
import org.tinygroup.dict.Dict;
import org.tinygroup.dict.DictManager;
import org.tinygroup.dict.impl.AbstractDictLoader;

import java.util.List;

/**
 * 数据字典管理器
 */
public class DictDataLoaderImpl extends AbstractDictLoader {
    private DictDataLoaderBusiness dictDataLoaderBusiness;

    public DictDataLoaderBusiness getDictDataLoaderBusiness() {
        return dictDataLoaderBusiness;
    }

    public void setDictDataLoaderBusiness(DictDataLoaderBusiness dictDataLoaderBusiness) {
        this.dictDataLoaderBusiness = dictDataLoaderBusiness;
    }

    public List<Dict> loadDict() {
        return dictDataLoaderBusiness.loadDict();
    }

    public void load(DictManager dictManager) {
        List<Dict> dictList = dictDataLoaderBusiness.loadDict();
        for (Dict dict : dictList) {
            putDict(dict.getName(), dict, dictManager);
        }
    }


}
