/**
 *
 * Copyright (c) 2014-2017 All Rights Reserved.
 */
package org.tinygroup.bizframe.service.inter;

import java.io.Serializable;

import org.tinygroup.bizframe.common.dto.PageRequest;
import org.tinygroup.bizframe.common.dto.PageResponse;

/**
 * 全文检索
 * @author zhangliang08072
 * @version $Id: FullTextService.java, v 0.1 2017-4-13 上午8:57:00 zhangliang08072 Exp $
 */
public interface FullTextService {
	/**
	 * 全文检索
	 * @param pageRequest
	 * @param searchText
	 * @return
	 */
	public PageResponse getPagerFullText(PageRequest pageRequest,String searchText,Class objClass,String tableName,String columnName,String fieldType,Boolean isIdString);
	
	public Serializable getBusinessObjectByKey(Class objClass, String id, Boolean isIdString,String tableName, String columnName);
	
	public void addFullTextIndex(String _id,String _type,String _abstract);
	
	public void deleteFullTextIndex(String _type,String[] idArray);
}
