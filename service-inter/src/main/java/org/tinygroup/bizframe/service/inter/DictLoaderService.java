package org.tinygroup.bizframe.service.inter;


import org.tinygroup.dict.Dict;

import java.util.List;

/**
 * Created by wangwy on 2016/11/30.
 */
public interface DictLoaderService {
    List<Dict> loadDict();
}
