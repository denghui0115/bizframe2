/**
 *
 * Copyright (c) 2014-2017 All Rights Reserved.
 */
package org.tinygroup.bizframe.business.impl;

import java.util.List;

import org.tinygroup.bizframe.business.inter.FullTextBusiness;
import org.tinygroup.bizframe.ext.dao.inter.CommonPersisentObjectDao;

/**
 * 
 * @author zhangliang08072
 * @version $Id: FullTextBusinessImpl.java, v 0.1 2017-4-13 上午9:57:21 zhangliang08072 Exp $
 */
public class FullTextBusinessImpl implements FullTextBusiness {
	private CommonPersisentObjectDao commonPersisentObjectDao;
	public CommonPersisentObjectDao getCommonPersisentObjectDao() {
		return commonPersisentObjectDao;
	}
	public void setCommonPersisentObjectDao(
			CommonPersisentObjectDao commonPersisentObjectDao) {
		this.commonPersisentObjectDao = commonPersisentObjectDao;
	}
	/** 
	 * @see org.tinygroup.bizframe.business.inter.FullTextBusiness#queryOjbListByIds(java.lang.Class, java.lang.String[], java.lang.String, java.lang.String)
	 */
	@Override
	public <T> List<T> queryOjbListByIds(Class<T> objClass, String[] idArray,Boolean isIdString,
			String tableName, String columnName) {
		return commonPersisentObjectDao.queryOjbListByIds(objClass, idArray,isIdString, tableName, columnName);
	}
	/** 
	 * @see org.tinygroup.bizframe.business.inter.FullTextBusiness#queryOjbById(java.lang.Class, java.lang.String, java.lang.Boolean, java.lang.String, java.lang.String)
	 */
	@Override
	public <T> T queryOjbById(Class<T> objClass, String id, Boolean isIdString,
			String tableName, String columnName) {
		return commonPersisentObjectDao.queryOjbById(objClass, id, isIdString, tableName, columnName);
	}

}
