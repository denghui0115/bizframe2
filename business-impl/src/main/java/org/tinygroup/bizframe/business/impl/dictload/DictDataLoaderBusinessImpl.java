package org.tinygroup.bizframe.business.impl.dictload;

import org.tinygroup.bizframe.business.inter.DictDataLoaderBusiness;
import org.tinygroup.bizframe.dao.inter.TsysDictEntryDao;
import org.tinygroup.bizframe.dao.inter.TsysDictItemDao;
import org.tinygroup.bizframe.dao.inter.pojo.TsysDictEntry;
import org.tinygroup.bizframe.dao.inter.pojo.TsysDictItem;
import org.tinygroup.dict.Dict;
import org.tinygroup.dict.DictGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据字典
 */
public class DictDataLoaderBusinessImpl implements DictDataLoaderBusiness {
	private TsysDictEntryDao tsysDictEntryDao;

	private TsysDictItemDao tsysDictItemDao;


	public TsysDictItemDao getTsysDictItemDao() {
		return tsysDictItemDao;
	}

	public void setTsysDictItemDao(TsysDictItemDao tsysDictItemDao) {
		this.tsysDictItemDao = tsysDictItemDao;
	}

	public TsysDictEntryDao getTsysDictEntryDao() {

		return tsysDictEntryDao;
	}

	public void setTsysDictEntryDao(TsysDictEntryDao tsysDictEntryDao) {
		this.tsysDictEntryDao = tsysDictEntryDao;
	}

	public List<Dict> loadDict() {
		List<Dict> dictList = new ArrayList<Dict>();
		List<TsysDictEntry> tsysDictEntryList = tsysDictEntryDao.query(null);
		if (tsysDictEntryList != null) {
			for (TsysDictEntry tDict : tsysDictEntryList) {
				Integer dictId = tDict.getId();
				TsysDictItem tsysDictItem = new TsysDictItem();
				tsysDictItem.setDictEntryId(dictId);
				//select * from t_dict_item where dict_id=?
				List<TsysDictItem> items = tsysDictItemDao.query(tsysDictItem);
				org.tinygroup.dict.Dict dict = new org.tinygroup.dict.Dict();
//					dict.setName((String) bean.get(DICT_VALUE));
				dict.setName(tDict.getDictEntryCode());
				DictGroup group = new DictGroup();
				if(items!=null){
					for (TsysDictItem item : items) {
						org.tinygroup.dict.DictItem dictItem = new org.tinygroup.dict.DictItem();
						dictItem.setText(item.getDictItemName());
						dictItem.setValue(item.getDictItemCode());
						group.addDictItem(dictItem);
					}
				}
				dict.addDictGroup(group);
				dictList.add(dict);
			}
		}
		return dictList;
	}

}
