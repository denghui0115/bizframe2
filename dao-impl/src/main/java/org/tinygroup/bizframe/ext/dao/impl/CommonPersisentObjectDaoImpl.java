/**
 *
 * Copyright (c) 2014-2017 All Rights Reserved.
 */
package org.tinygroup.bizframe.ext.dao.impl;

import java.util.List;

import org.tinygroup.bizframe.ext.dao.inter.CommonPersisentObjectDao;
import org.tinygroup.jdbctemplatedslsession.daosupport.TinyDslDaoSupport;
import org.tinygroup.tinysqldsl.Select;
import static org.tinygroup.tinysqldsl.expression.FragmentExpressionSql.*;
import static org.tinygroup.tinysqldsl.Select.select;

/**
 * 
 * @author zhangliang08072
 * @version $Id: CommonDaoImpl.java, v 0.1 2017-4-13 上午12:42:57 zhangliang08072 Exp $
 */
public class CommonPersisentObjectDaoImpl extends TinyDslDaoSupport implements CommonPersisentObjectDao {

	/** 
	 * @see org.tinygroup.bizframe.ext.dao.inter.CommonPersisentObjectDao#queryOjbListByIds(java.lang.Class, java.lang.String[], java.lang.String, java.lang.String)
	 */
	@Override
	public <T> List<T> queryOjbListByIds(Class<T> objClass,String[] idArray,Boolean isIdString, String tableName, String columnName) {
		if(isIdString == null){
			isIdString = false;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for(int i=0;i<idArray.length;i++){
			if(isIdString){
				sb.append("'").append(idArray[i]).append("',");
			}else{
				sb.append(idArray[i]).append(",");
			}
		}
		if(sb.length()-1>=1){
		    sb.deleteCharAt(sb.length()-1);
		}
		sb.append(")");
		Select select = select(fragmentSelect("t.*")).from(fragmentFrom(tableName+" t"))
				.where(fragmentCondition("t."+columnName+" in "+sb.toString()));
		return getDslSession().fetchList(select,objClass);
	}

	/** 
	 * @see org.tinygroup.bizframe.ext.dao.inter.CommonPersisentObjectDao#queryOjbById(java.lang.Class, java.lang.String, java.lang.Class, java.lang.String, java.lang.String)
	 */
	@Override
	public <T> T queryOjbById(Class<T> objClass, String id, Boolean isIdString,String tableName, String columnName) {
		if(isIdString == null){
			isIdString = false;
		}
		Object oId = id;
		if(!isIdString){
			oId = Integer.parseInt(id);
		}
		Select select = select(fragmentSelect("t.*")).from(fragmentFrom(tableName+" t"))
				.where(fragmentCondition("t."+columnName+" = ? ",oId));
		return getDslSession().fetchOneResult(select, objClass);
	}

}
