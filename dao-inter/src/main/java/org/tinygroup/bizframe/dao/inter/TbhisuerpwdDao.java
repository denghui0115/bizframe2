package org.tinygroup.bizframe.dao.inter;

import org.tinygroup.bizframe.dao.inter.pojo.Tbhisuerpwd;
import org.tinygroup.jdbctemplatedslsession.daosupport.BaseDao;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
public interface TbhisuerpwdDao extends BaseDao<Tbhisuerpwd, Integer> {

}
