package org.tinygroup.bizframe.dao.inter;

import java.util.List;

import org.tinygroup.bizframe.dao.inter.pojo.TreeData;
import org.tinygroup.bizframe.dao.inter.pojo.TsysMenu;
import org.tinygroup.jdbctemplatedslsession.daosupport.BaseDao;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

/**
 * <!-- begin-user-doc --> 如果不希望某方法或者变量被覆盖，可以在方法或者变量注释中增加@unmodifiable <!--
 * end-user-doc -->
 */
public interface TsysMenuDao extends BaseDao<TsysMenu, Integer> {

	/**
	 * 
	 * @param tree
	 * @return
	 * @unmodifiable
	 */
	List getMenuTree(TreeData tree);

	/**
	 * 
	 * @param start
	 * @param limit
	 * @param t
	 * @param orderArgs
	 * @return
	 * @unmodifiable
	 */
	Pager<TsysMenu> queryPagerForSearch(int start, int limit, TsysMenu t,
			OrderBy... orderArgs);

	/**
	 * 
	 * @param t
	 * @return
	 * @unmodifiable
	 */
	List checkExist(TsysMenu t);

	/**
	 * 
	 * @param userCode
	 * @param orderArgs
	 * @return
	 * @unmodifiable
	 */
	List<Integer> findMenuIdsByUser(String userCode, OrderBy... orderArgs);

}
