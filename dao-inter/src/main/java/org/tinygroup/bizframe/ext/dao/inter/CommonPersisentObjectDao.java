package org.tinygroup.bizframe.ext.dao.inter;

import java.util.List;

public interface CommonPersisentObjectDao {
	/**
	 * 
	 * @param objClass 返回的持久化对象的Class类型
	 * @param idArray  持久化对象的主键数组
	 * @param isIdString 持久化对象的主键是否是字符串类型（varchar）
	 * @param tableName 持久化对象对应的表
	 * @param columnName 持久化对象的主键对应的字段名称
	 * @return
	 */
	public <T> List<T> queryOjbListByIds(Class<T> objClass ,String[] idArray,Boolean isIdString,String tableName,String columnName);
	
	/**
	 * 
	 * @param objClass 返回的持久化对象的Class类型
	 * @param id 持久化对象的主键
	 * @param isIdString 持久化对象的主键是否是字符串类型（varchar）
	 * @param tableName 持久化对象对应的表
	 * @param columnName 持久化对象的主键对应的字段名称
	 * @return
	 */
	public <T> T queryOjbById(Class<T> objClass ,String id,Boolean isIdString,String tableName,String columnName);
}
