package org.tinygroup.bizframe.ext.dao.inter;


import org.tinygroup.bizframe.dao.inter.pojo.complex.TRightRef;
import org.tinygroup.tinysqldsl.Pager;

/**
 * Created by Administrator on 2016/10/26.
 */
public interface TUserRightRefDealDao {

    /**
     *
     * @param userId
     * @param menuIds
     * @param createBy
     * @return
     * @unmodifiable
     */
    int[] grantRights(String userId,Integer[] menuIds, String createBy);

    /**
     *
     * @param userCode
     * @param menuIds
     * @return
     * @unmodifiable
     */
    int revokeRights(String userCode,Integer... menuIds);

    /**
     *
     * @param start
     * @param pageSize
     * @param userId
     * @param tRightRef
     * @param isAssigned
     * @return
     * @unmodifiable
     */
    Pager<TRightRef> queryRightsInUser(int start, int pageSize,
                                       String userId, TRightRef tRightRef, final Boolean isAssigned);

}
