package org.tinygroup.bizframe.ext.dao.inter;

import org.tinygroup.bizframe.dao.inter.pojo.complex.TRightRef;
import org.tinygroup.tinysqldsl.Pager;

/**
 * Created by Administrator on 2016/10/26.
 */
public interface TRoleRightRefDealDao {
    /**
     *
     * @param start
     * @param pageSize
     * @param roleId
     * @param tRightRef
     * @param isAssigned
     * @return
     * @unmodifiable
     */
    Pager queryRightsInRole(int start, int pageSize, Integer roleId, TRightRef tRightRef, Boolean isAssigned);

    /**
     *
     * @param roleId
     * @param menuIds
     * @param createBy
     * @return
     * @unmodifiable
     */
    int[] grantRights(Integer roleId,Integer[] menuIds, String createBy);

    /**
     *
     * @param roleId
     * @param menuIds
     * @return
     * @unmodifiable
     */
    int revokeRights(Integer roleId,Integer[] menuIds);
}
